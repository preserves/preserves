---
title: preserves-tool
---

The `preserves-tool` program is a swiss army knife for working with
Preserves documents.

```
preserves-tool 4.992.0
Swiss-army knife tool for working with Preserves data.
See https://preserves.dev/. If no subcommand is specified, the default
subcommand will be `convert`.

USAGE:
    preserves-tool [OPTIONS]
    preserves-tool <SUBCOMMAND>

OPTIONS:
    -h, --help       Print help information
    -V, --version    Print version information

OPTIONS FOR DEFAULT SUBCOMMAND convert:
    [...]

SUBCOMMANDS:
    completions
    convert
    help           Print this message or the help of the given subcommand(s)
    quote
```

## Installation

The tool is
[written in Rust](https://crates.io/crates/preserves-tools).
[Install `cargo`.](https://doc.rust-lang.org/cargo/getting-started/installation.html)
Then, `cargo install preserves-tools`.

## Subcommands

The tool includes three subcommands.

### `preserves-tool convert`, `preserves-tool`

This is the main tool, and is also the default if no subcommand is
explicitly specified. It can

 - translate between the various Preserves text and binary document
   syntaxes;
 - strip annotations;
 - pretty-print; and
 - break down and filter documents using [preserves path](..{% link preserves-path.md %}) selectors.

#### Usage

    preserves-tool-convert

    USAGE:
        preserves-tool convert [FLAGS] [OPTIONS]

    OPTIONS:
            --bundle <filename>


        -c, --commas <COMMAS>
                [default: none] [possible values: none, separating, terminating]

            --collect


            --escape-spaces


        -h, --help
                Print help information

        -i, --input-format <INPUT_FORMAT>
                [default: auto-detect] [possible values: auto-detect, text, binary]

            --indent <on/off>
                [default: on] [possible values: disabled, enabled]

            --limit <LIMIT>


        -o, --output-format <OUTPUT_FORMAT>
                [default: text] [possible values: text, binary, unquoted]

            --read-annotations <on/off>
                [default: on] [possible values: disabled, enabled]

            --select <SELECT_EXPR>
                [default: *]

            --select-output <SELECT_OUTPUT>
                [default: sequence] [possible values: sequence, set]

            --write-annotations <on/off>
                [default: on] [possible values: disabled, enabled]

### `preserves-tool quote`

This subcommand reads chunks from standard input and outputs each one
as a Preserves `String`, `Symbol`, or `ByteString` using either the
text or binary Preserves surface syntax.

This is useful when writing shell scripts that interact with other
programs using Preserves as an interchange format.

It defaults to taking the entirety of standard input as a single large
chunk, but it can also work with newline- or `nul`-delimited chunks.

#### Usage

```
preserves-tool-quote

USAGE:
    preserves-tool quote [OPTIONS] <SUBCOMMAND>

OPTIONS:
    -h, --help                             Print help information
    -o, --output-format <OUTPUT_FORMAT>    [default: text] [possible values: text,
                                           binary, unquoted]

SUBCOMMANDS:
    byte-string
    help           Print this message or the help of the given subcommand(s)
    string
    symbol
```

```
preserves-tool-quote-string

USAGE:
    preserves-tool quote string [OPTIONS]

OPTIONS:
        --escape-spaces
    -h, --help                                   Print help information
        --include-terminator
        --input-terminator <INPUT_TERMINATOR>    [default: eof] [possible values:
                                                 eof, newline, nul]
```

```
preserves-tool-quote-symbol

USAGE:
    preserves-tool quote symbol [OPTIONS]

OPTIONS:
        --escape-spaces
    -h, --help                                   Print help information
        --include-terminator
        --input-terminator <INPUT_TERMINATOR>    [default: eof] [possible values:
                                                 eof, newline, nul]
```

```
preserves-tool-quote-byte-string

USAGE:
    preserves-tool quote byte-string

OPTIONS:
    -h, --help    Print help information
```

### `preserves-tool completions`

This subcommand outputs Bash completion code to stdout, for sourcing
at shell startup time.

#### Usage

Add the following to your `.profile` or similar:

    eval "$(preserves-tool completions bash 2>/dev/null)"

Multiple shell dialects are supported (courtesy of
[`clap`](https://crates.io/crates/clap)):

```
preserves-tool-completions

USAGE:
    preserves-tool completions <SHELL>

ARGS:
    <SHELL>    [possible values: bash, elvish, fish, powershell, zsh]

OPTIONS:
    -h, --help    Print help information
```
