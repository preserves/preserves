A Preserves schema connects Preserves `Value`s to host-language data
structures. Each definition within a schema can be processed by a
compiler to produce

 - a simple host-language *type definition*;

 - a partial *parsing* function from `Value`s to instances of the
   produced type; and

 - a total *serialization* function from instances of the type to
   `Value`s.

Every parsed `Value` retains enough information to always be able to
be serialized again, and every instance of a host-language data
structure contains, by construction, enough information to be
successfully serialized.
