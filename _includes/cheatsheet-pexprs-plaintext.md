The definitions of `Atom`, `ws`, and `linecomment` are as given in the Preserves text syntax.

```text
Document      :=  Expr* Trailer ws
Expr          :=  ws (SimpleExpr | Punct)
SimpleExpr    :=  Compound | Embedded | Annotated | Atom
Compound      :=  Sequence | Record | Block | Group | Set
Punct         :=  `,` | `;` | `:`+

Sequence      :=   `[` Expr* Trailer ws `]`
Record        :=   `<` Expr* Trailer ws `>`
Block         :=   `{` Expr* Trailer ws `}`
Group         :=   `(` Expr* Trailer ws `)`
Set           :=  `#{` Expr* Trailer ws `}`

Trailer       :=  (ws Annotation)*

Embedded      :=  `#:` SimpleExpr
Annotated     :=  Annotation SimpleExpr
Annotation    :=  `@` SimpleExpr | `#` ((space | tab | `!`) linecomment) (cr | lf)
```
