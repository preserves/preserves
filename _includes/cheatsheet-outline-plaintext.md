```text
Document    :=  Outline<0> (nl `---` nl «any unicode scalar value»*)?

Outline<n>  :=  Indent<n>? hws SimpleValue
             |  hws `>` Line | (Indent<n> `>` Line)*
             |  (Indent<n> SimpleValue hws `:` Outline<(m > n)>)+
             |  (Indent<n> `-` Outline<(m > n)>)+
             |  hws «any unicode scalar except cr or lf»*

SimpleValue :=  Record | Collection
             |  Boolean | String | ByteString | QuotedSymbol
             |  Double | SignedInteger
             |  Embedded

Indent<n>   :=  nl+ space{n}

Line        :=  «any unicode scalar except cr or lf»*

hws         :=  (space | tab)*
nl          :=  hws cr? lf
```
