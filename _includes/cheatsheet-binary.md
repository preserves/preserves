For a value <span class="postcard-grammar binarysyntax">*V*</span>, we write <span
class="postcard-grammar binarysyntax">«*V*»</span> for the binary encoding of <span
class="postcard-grammar binarysyntax">*V*</span>.

{:.postcard-grammar.binarysyntax}
«`#f`» | = | `80`
«`#t`» | = | `81`

{:.postcard-grammar.binarysyntax}
«`@`*W* *V*» | = | `85` «*W*» «*V*»
«`#:`*V*» | = | `86` «*V*»

{:.postcard-grammar.binarysyntax}
«*V*» | = | `87``08` **binary64**(*V*) | if *V* ∈ Double

{:.postcard-grammar.binarysyntax}
«*V*» | = | `B0` **varint**(&#124;**intbytes**(*V*)&#124;) **intbytes**(*V*) | if *V* ∈ SignedInteger
«*V*» | = | `B1` **varint**(&#124;**utf8**(*V*)&#124;) **utf8**(*V*)         | if *V* ∈ String
«*V*» | = | `B2` **varint**(&#124;*V*&#124;) *V*                             | if *V* ∈ ByteString
«*V*» | = | `B3` **varint**(&#124;**utf8**(*V*)&#124;) **utf8**(*V*)         | if *V* ∈ Symbol

{:.postcard-grammar.binarysyntax}
«`<`*L* *F*<sub>1</sub> ... *F*<sub>m</sub>`>`» | = | `B4` «*L*» «*F*<sub>1</sub>» ... «*F*<sub>m</sub>» `84`
«`[`*X*<sub>1</sub> ... *X*<sub>m</sub>`]`» | = | `B5` «*X*<sub>1</sub>» ... «*X*<sub>m</sub>» `84`
«`#{`*E*<sub>1</sub> ... *E*<sub>m</sub>`}`» | = | `B6` «*E*<sub>1</sub>» ... «*E*<sub>m</sub>» `84`
«`{`*K*<sub>1</sub>`:`*V*<sub>1</sub> ... *K*<sub>m</sub>`:`*V*<sub>m</sub>`}`» | = | `B7` «*K*<sub>1</sub>» «*V*<sub>1</sub>» ... «*K*<sub>m</sub>» «*V*<sub>m</sub>» `84`

{:.postcard-grammar.binarysyntax}
**varint**(*n*) | = | <span class="outputish">*n*</span> | if *n* &lt; 128
| | <span class="outputish">(*n* &amp; 127) &#124; 128</span> **varint**(*n* &gt;&gt; 7) | if *n* ≥ 128

{:.postcard-grammar.binarysyntax}
**intbytes**(*n*) | = | <span class="roman">the empty sequence if</span> *n* = 0<span class="roman">, otherwise</span> **signedBigEndian**(*n*)

{:.postcard-grammar.binarysyntax}
**signedBigEndian**(*n*) | = | <span class="outputish">*n* &amp; 255</span> | if −128 ≤ *n* ≤ 127
| | **signedBigEndian**(*n* &gt;&gt; 8) <span class="outputish">*n* &amp; 255</span> | otherwise

The function <span class="postcard-grammar binarysyntax">**binary64**(*D*)</span> yields the
big-endian 8-byte IEEE 754 binary representation of <span class="postcard-grammar
binarysyntax">*D*</span>.

<!--
Together, <span class="postcard-grammar binarysyntax">**div**</span> and <span
class="postcard-grammar binarysyntax">**mod**</span> give [Euclidean
division](https://en.wikipedia.org/wiki/Euclidean_division); that is, if
<span class="postcard-grammar binarysyntax">*n* **div** *d* = *q*</span> and
<span class="postcard-grammar binarysyntax">*n* **mod** *d* = *r*</span>, then
<span class="postcard-grammar binarysyntax">*n* = *dq* + *r*</span> and
<span class="postcard-grammar binarysyntax">0 ≤ *r* &lt; |d|</span>.
-->
