The definitions of `Atom`, `ws`, and `linecomment` are as given in the Preserves text syntax.

{:.postcard-grammar.textsyntax}
| *Document*      | := |  *Expr*<sup>⋆</sup> *Trailer* **ws**
| *Expr*          | := |  **ws** (*SimpleExpr* &#124; *Punct*)
| *SimpleExpr*    | := |  *Compound* &#124; *Embedded* &#124; *Annotated* &#124; *Atom*
| *Compound*      | := |  *Sequence* &#124; *Record* &#124; *Block* &#124; *Group* &#124; *Set*
| *Punct*         | := |  `,` &#124; `;` &#124; `:`<sup>+</sup>

{:.postcard-grammar.textsyntax}
| *Sequence*      | := |   `[` *Expr*<sup>⋆</sup> *Trailer* **ws** `]`
| *Record*        | := |   `<` *Expr*<sup>⋆</sup> *Trailer* **ws** `>`
| *Block*         | := |   `{` *Expr*<sup>⋆</sup> *Trailer* **ws** `}`
| *Group*         | := |   `(` *Expr*<sup>⋆</sup> *Trailer* **ws** `)`
| *Set*           | := |  `#{` *Expr*<sup>⋆</sup> *Trailer* **ws** `}`

{:.postcard-grammar.textsyntax}
| *Trailer*       | := |  (**ws** *Annotation*)<sup>⋆</sup>

{:.postcard-grammar.textsyntax}
| *Embedded*      | := |  `#:` *SimpleExpr*
| *Annotated*     | := |  *Annotation* *SimpleExpr*
| *Annotation*    | := |  `@` *SimpleExpr* &#124; `#` ((**space** &#124; **tab** &#124; `!`) *linecomment*) (**cr** &#124; **lf**)
