```text
Document      :=  Value ws
Value         :=  ws (Record | Collection | Embedded | Annotated | Atom)
Collection    :=  Sequence | Dictionary | Set

Record        :=  `<` Value+ ws `>`
Sequence      :=  `[` (commas Value)* commas `]`
Set           :=  `#{` (commas Value)* commas `}`
Dictionary    :=  `{` (commas Value ws `:` Value)* commas `}`
commas        :=  (ws `,`)* ws

Embedded      :=  `#:` Value
Annotated     :=  Annotation Value
Annotation    :=  `@` Value | `#` ((space | tab | `!`) linecomment) (cr | lf)

Atom          :=  Boolean | ByteString | String | QuotedSymbol | Symbol | Number
Boolean       :=  `#t` | `#f`
ByteString    :=  `#"` binchar* `"`
               |  `#x"` (ws hex hex)* ws `"`
               |  `#[` (ws base64char)* ws `]`
String        :=  `"` («any unicode scalar except `\` or `"`» | escaped | `\"`)* `"`
QuotedSymbol  :=  `'` («any unicode scalar except `\` or `'`» | escaped | `\'`)* `'`
Symbol        :=  (`A`..`Z` | `a`..`z` | `0`..`9` | sympunct | symuchar)+
Number        :=  Double | SignedInteger
Double        :=  flt | `#xd"` (ws hex hex)8 ws `"`
SignedInteger :=  int

escaped       :=  `\\` | `\/` | `\b` | `\f` | `\n` | `\r` | `\t` | `\u` hex hex hex hex
binescaped    :=  `\\` | `\/` | `\b` | `\f` | `\n` | `\r` | `\t` | `\x` hex hex
binchar       :=  «any scalar ≥32 and ≤126, except `\` or `"`» | binescaped | `\"`
base64char    :=  `A`..`Z` | `a`..`z` | `0`..`9` | `+` | `/` | `-` | `_` | `=`
sympunct      :=  `~` | `!` | `$` | `%` | `^` | `&` | `*` | `?`
               |  `_` | `=` | `+` | `-` | `/` | `.` | `|`
symuchar      :=  «any scalar value ≥128 whose Unicode category is
                   Lu, Ll, Lt, Lm, Lo, Mn, Mc, Me, Nd, Nl, No, Pc,
                   Pd, Po, Sc, Sm, Sk, So, or Co»

flt           :=  int ( frac exp | frac | exp )
int           :=  (`-`|`+`) (`0`..`9`)+
frac          :=  `.` (`0`..`9`)+
exp           :=  (`e`|`E`) (`-`|`+`) (`0`..`9`)+
hex           :=  `A`..`F` | `a`..`f` | `0`..`9`

ws            :=  (space | tab | cr | lf)*
delimiter     :=  ws | `<` | `>` | `[` | `]` | `{` | `}`
                     | `#` | `:` | `"` | `'` | `@` | `;` | `,`
linecomment   :=  «any unicode scalar except cr or lf»*
```
