{:.postcard-grammar.textsyntax}
| *Document* | := | *Value* **ws** |
| *Value* | := | **ws** (*Record* &#124; *Collection* &#124; *Embedded* &#124; *Annotated* &#124; *Atom*) |
| *Collection* | := | *Sequence* &#124; *Dictionary* &#124; *Set* |

{:.postcard-grammar.textsyntax}
| *Record* | := | `<`*Value*<sup>+</sup> **ws**`>` |
| *Sequence* | := | `[`(**commas** *Value*)<sup>⋆</sup> **commas**`]` |
| *Set* | := | `#{`(**commas** *Value*)<sup>⋆</sup> **commas**`}` |
| *Dictionary* | := | `{` (**commas** *Value* **ws**`:`*Value*)<sup>⋆</sup> **commas**`}` |
| **commas** | := | (**ws** `,`)<sup>⋆</sup> **ws** |

{:.postcard-grammar.textsyntax}
| *Embedded* | := | `#:`*Value* |
| *Annotated* | := | *Annotation* *Value* |
| *Annotation* | := | `@`*Value* &#124;`#` ((**space** &#124; **tab** &#124; `!`) *linecomment*) (**cr** &#124; **lf**) |

{:.postcard-grammar.textsyntax}
| *Atom* | := | *Boolean* &#124; *ByteString* &#124; *String* &#124; *QuotedSymbol* &#124; *Symbol* &#124; *Number* |
| *Boolean* | := | `#t`&#124;`#f` |
| *ByteString* | := | `#"`*binchar*<sup>⋆</sup> `"`&#124;`#x"` (**ws** *hex* *hex*)<sup>⋆</sup> **ws**`"`&#124;`#[` (**ws** *base64char*)<sup>⋆</sup> **ws**`]` |
| *String* | := | `"` (« any unicode scalar value except `\` or `"` » &#124; *escaped* &#124;`\"`)<sup>⋆</sup> `"` |
| *QuotedSymbol* | := | `'` (« any unicode scalar value except `\` or `'` » &#124; *escaped* &#124;`\'`)<sup>⋆</sup> `'` |
| *Symbol* | := | (`A`..`Z`&#124;`a`..`z`&#124;`0`..`9`&#124; *sympunct* &#124; *symuchar*)<sup>+</sup> |
| *Number* | := | *Double* &#124; *SignedInteger* |
| *Double* | := | *flt* &#124;`#xd"` (**ws** *hex* *hex*)<sup>8</sup> **ws**`"` |
| *SignedInteger* | := | *int* |

{:.postcard-grammar.textsyntax}
| *escaped* | := | `\\`&#124;`\/`&#124;`\b`&#124;`\f`&#124;`\n`&#124;`\r`&#124;`\t`&#124;`\u`*hex* *hex* *hex* *hex* |
| *binescaped* | := | `\\`&#124;`\/`&#124;`\b`&#124;`\f`&#124;`\n`&#124;`\r`&#124;`\t`&#124;`\x`*hex* *hex* |
| *binchar* | := | « any unicode scalar value ≥32 and ≤126, except `\` or `"` » &#124; *binescaped* &#124;`\"` |
| *base64char* | := | `A`..`Z`&#124;`a`..`z`&#124;`0`..`9`&#124;`+`&#124;`/`&#124;`-`&#124;`_`&#124;`=` |
| *sympunct* | := | `~`&#124;`!`&#124;`$`&#124;`%`&#124;`^`&#124;`&`&#124;`*`&#124;`?`&#124;`_`&#124;`=`&#124;`+`&#124;`-`&#124;`/`&#124;`.`&#124;`|` |
| *symuchar* | := | « any scalar value ≥128 whose Unicode category is Lu, Ll, Lt, Lm, Lo, Mn, Mc, Me, Nd, Nl, No, Pc, Pd, Po, Sc, Sm, Sk, So, or Co » |

{:.postcard-grammar.textsyntax}
| *flt* | := | *int* ( *frac* *exp* &#124; *frac* &#124; *exp* ) |
| *int* | := | (`-`&#124;`+`) (`0`..`9`)<sup>+</sup> |
| *frac* | := | `.` (`0`..`9`)<sup>+</sup> |
| *exp* | := | (`e`&#124;`E`) (`-`&#124;`+`) (`0`..`9`)<sup>+</sup> |
| *hex* | := | `A`..`F`&#124;`a`..`f`&#124;`0`..`9` |

{:.postcard-grammar.textsyntax}
| **ws** | := | (**space** &#124; **tab** &#124; **cr** &#124; **lf**)<sup>⋆</sup> |
| **delimiter** | := | **ws** &#124; `<` &#124; `>` &#124; `[` &#124; `]` &#124; `{` &#124; `}` &#124; `#` &#124; `:` &#124; `"` &#124; `'` &#124; `@` &#124; `;` &#124; `,` |
| *linecomment* | := | « any unicode scalar value except **cr** or **lf** »<sup>⋆</sup> |
