---
title: "Open questions"
---

Q. Should "symbols" instead be URIs? Relative, usually; relative to
what? Some domain-specific base URI?

> No. They may be interpreted as URIs, of course; see
> [here](preserves.html#fn:iri-labels).

Q. Literal small integers: are they pulling their weight? They're not
absolutely necessary. A. No, they have been removed (as part of the changes
at version 0.990).

> No. They were removed in the simplification of the syntax that was the
> outcome of [issue
> 41](https://gitlab.com/preserves/preserves/-/issues/41).

Q. Should we go for trying to make the data ordering line up with the
encoding ordering? We'd have to only use streaming forms, and avoid
the small integer encoding, and not store record arities, and sort
sets and dictionaries, and mask doubles (perhaps
[like this](https://stackoverflow.com/questions/43299299/sorting-floating-point-values-using-their-byte-representation)),
and perhaps pick a specific `NaN`, and I don't know what to do about
SignedIntegers. Perhaps make them more like floating-point formats, with the
byte count acting as a kind of exponent underneath the sign bit.

 - Perhaps define separate additional canonicalization restrictions?
   Doesn't help the ordering, but does help the equivalence.

 - Canonicalization and early-bailout-equivalence-checking are in
   tension with support for streaming values.

Q. Should `IOList`s be wrapped in an identifying unary record constructor?

Q. Whitespace - is having `,` as whitespace sensible or not? I can
think of arguments both ways. Similarly, JSON *benefits* from
requiring a comma separating each item in a list; we currently don't
require any whitespace at all between elements of a list, making it
ambiguous: does `[123]` denote a single-element or a three-element
list? Compare JSON where `[1,2,3]` is unambiguously different from
`[123]`.

> With the addition of the notion of
> [delimiters](preserves-text.html#delimiters) to the text syntax, we at
> least answer the question of how `[123]` parses: it must yield a
> single-element list.
