---
title: "Preserves: Media Types and File Extensions"
---

Tony Garnock-Jones <tonyg@leastfixedpoint.com>  
{{ site.version_date }}. Version {{ site.version }}.

*Preserves* is a data model, with associated serialization formats. This document defines
[media types][media type] (also known as MIME types) for different representations of Preserves
data.

  [media type]: https://www.iana.org/assignments/media-types/media-types.xhtml
  [machine-oriented binary syntax]: preserves-binary.html
  [textual syntax]: preserves-text.html
  [schema]: preserves-schema.html

## Media Types

The Preserves [data model](preserves.html) has multiple different serialization formats. Each
has its own [media type][]. (*These media types are unofficial: they are not yet registered
with IANA.*)

 - `application/preserves`: Preserves data represented using the [machine-oriented binary
   syntax][]

 - `text/preserves`: Preserves data represented using the [textual syntax][]

## File extensions

 - `.pr`: General-purpose Preserves data, represented using the [textual syntax][].

 - `.prb`: General-purpose Preserves data, represented using the [machine-oriented binary
   syntax][].

    - Often used for the output of [schema][] compilation; sometimes, `.bin` is seen in this
      role; prefer `.prb`.

 - `.prs`: Preserves [textual syntax][] representing a [schema][] document.
