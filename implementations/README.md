# Preserves Implementations

Here you may find:

 - [dhall](dhall/), functions for converting Dhall values to a corresponding
   subset of Preserves.

 - [javascript](javascript/), an implementation in TypeScript,
   compiling to JavaScript, for node.js and the Browser.

 - [python](python/), an implementation for Python 2.x and 3.x.

 - [racket](racket/), an implementation for Racket 7.x and newer
   (though older Rackets may also work with it).

Other implementations are also available:

 - [Preserves for Rust](https://gitlab.com/preserves/preserves-rs/), an implementation for Rust
   that interoperates with serde.

 - [Preserves for Squeak Smalltalk](https://squeaksource.com/Preserves.html)
