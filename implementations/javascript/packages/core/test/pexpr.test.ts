import { Pexpr, Value, fromJS, parse, stringify } from '../src/index';
import './test-utils';

function P(s: string): Value<any>[] {
    return parse(s, { includeAnnotations: true });
}

describe('basics', () => {
    it('simple example', () => {
        const r = new Pexpr.Reader('#!foo\n<bar {zot ::quux}, [a; b; c;]>');
        const d = r.nextDocument();
        expect(fromJS(d)).is(P(`[\n#!foo\n
          <r bar <b zot <p '::'> quux> <p ','> [a <p ';'> b <p ';'> c <p ';'>]>]`));
    });

    it('simple group', () => {
        const r = new Pexpr.Reader('(+ 1 2)');
        expect(fromJS(r.nextDocument())).is(P('[<g + 1 2>]'));
    });

    it('asPreserves', () => {
        const s = '{a: b, c: d, e: [1, 2 <r 3 4>]}';
        const d = new Pexpr.Reader(s).nextDocument();
        const v = Pexpr.asPreserves(d.get(0)!);
        expect(v).is(P(s));
    });
});

describe('trailing comments', () => {
    it('basics 1', () => {
        const d = new Pexpr.Reader('# a comment with nothing after').nextDocument();
        expect(d.annotations?.[d.exprs.length].get(0)?.item).toBe('a comment with nothing after');
    });

    it('basics 2', () => {
        const d = new Pexpr.Reader('# a comment with nothing after\n').nextDocument();
        expect(d.annotations?.[d.exprs.length].get(0)?.item).toBe('a comment with nothing after');
    });

    it('inside a sequence', () => {
        const d = new Pexpr.Reader('[\n1\n# a comment with nothing after\n]\n').nextDocument();
        const seq = d.get(0)?.item as Pexpr.Compound;
        expect(seq.annotations?.[seq.exprs.length].get(0)?.item).toBe('a comment with nothing after');
    });
});
