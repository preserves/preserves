import { Double, fromJS, IDENTITY_FOLD, fold, mapEmbeddeds, Value, preserves, KeyedDictionary, Embeddable, Embedded } from '../src/index';
import './test-utils';

describe('Double', () => {
    it('should print reasonably', () => {
        expect(Double(123.45).toString()).toEqual("123.45");
  });
});

describe('fold', () => {
    function mkv<T extends Embeddable>(t: T): Value<T> {
        return fromJS<T>([
            1,
            2,
            new KeyedDictionary<T>([[[3, 4], fromJS([5, 6])],
                                    ['a', 1],
                                    ['b', true]]),
            Double(3.4),
            t,
        ]);
    }

    it('should support identity', () => {
        const w = new Date();
        const v = mkv(new Embedded(w));
        expect(fold(v, IDENTITY_FOLD)).is(v);
        const w1 = new Date();
        const v1 = mkv(new Embedded(w1));
        expect(fold(v, IDENTITY_FOLD)).not.is(v1);
        expect(mapEmbeddeds(v, _t => new Embedded(w1))).is(v1);
    });
});

describe('fromJS', () => {
    it('should map integers to themselves', () => {
        expect(fromJS(1)).toBe(1);
    });

    it('should map bigints to themselves', () => {
        expect(fromJS(BigInt("12345678123456781234567812345678")))
            .toBe(BigInt("12345678123456781234567812345678"));;
    });
});

describe('is()', () => {
    it('should compare small integers sensibly', () => {
        expect(3).is(3);
        expect(3).not.is(4);
    });
    it('should compare large integers sensibly', () => {
        const a = BigInt("12345678123456781234567812345678");
        const b = BigInt("12345678123456781234567812345679");
        expect(a).is(a);
        expect(a).is(BigInt("12345678123456781234567812345678"));
        expect(a).not.is(b);
    });
    it('should compare mixed integers sensibly', () => {
        const a = BigInt("12345678123456781234567812345678");
        const b = BigInt("3");
        const c = BigInt("4");
        expect(3).not.is(a);
        expect(a).not.is(3);
        expect(3).not.toBe(b);
        expect(3).is(b);
        expect(b).not.toBe(3);
        expect(b).is(3);
        expect(3).not.toBe(c);
        expect(3).not.is(c);
        expect(c).not.toBe(3);
        expect(c).not.is(3);
    });
    it('should compare equivalent JsDictionary and KeyedDictionary values sensibly', () => {
        const a = {a: 1, b: 2};
        const b = new KeyedDictionary(
            [[Symbol.for('a'), 1], [Symbol.for('b'), 2]] as [Value, Value][]);
        expect(a).is(b);
    });
});

describe('`preserves` formatter', () => {
    it('should format numbers', () => {
        expect(preserves`>${3}<`).toBe('>3<');
    });
    it('should format small bigints', () => {
        expect(preserves`>${BigInt("3")}<`).toBe('>3<');
    });
    it('should format big bigints', () => {
        expect(preserves`>${BigInt("12345678123456781234567812345678")}<`)
            .toBe('>12345678123456781234567812345678<');
    });
    it('should format regular JS objects', () => {
        expect(preserves`>${({a: 1, b: 2})}<`)
            .toBe('>{a: 1 b: 2}<');
    });
    it('should format dictionaries with string keys', () => {
        const v = new KeyedDictionary([["a", 1], ["b", 2]]);
        expect(preserves`>${v}<`)
            .toBe('>{"a": 1 "b": 2}<');
    });
    it('should format dictionaries with symbol keys', () => {
        const v = new KeyedDictionary([[Symbol.for("a"), 1], [Symbol.for("b"), 2]]);
        expect(preserves`>${v}<`)
            .toBe('>{a: 1 b: 2}<');
    });
});
