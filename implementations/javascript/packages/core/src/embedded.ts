import type { DecoderState } from "./decoder";
import type { EncoderState } from "./encoder";
import type { Value } from "./values";
import { ReaderStateOptions } from "./reader";

export const IsEmbedded = Symbol.for('IsEmbedded');

export interface Embeddable {
    readonly [IsEmbedded]: true;
}

export function isEmbedded<T extends Embeddable>(v: any): v is T {
    return !!v?.[IsEmbedded];
}

export type EmbeddedTypeEncode<T extends Embeddable> = {
    encode(s: EncoderState, v: T): void;
}

export type EmbeddedTypeDecode<T> = {
    decode(s: DecoderState): T;
    fromValue(v: Value<GenericEmbedded>, options: ReaderStateOptions): T;
}

export type EmbeddedType<T extends Embeddable> = EmbeddedTypeEncode<T> & EmbeddedTypeDecode<T>;

export class Embedded<T> {
    get [IsEmbedded](): true { return true; }

    constructor(public readonly value: T) {}

    equals(other: any): boolean {
        return typeof other === 'object' && 'value' in other && Object.is(this.value, other.value);
    }
}

export class GenericEmbedded {
    get [IsEmbedded](): true { return true; }

    constructor(public readonly generic: Value) {}

    equals(other: any, is: (a: any, b: any) => boolean) {
        return typeof other === 'object' && 'generic' in other && is(this.generic, other.generic);
    }

    toString(): string {
        return this.generic.toString();
    }
}
