import { isEmbedded } from './embedded';
import { Equivalence, _iterMap } from './flex';

export interface JsDictionary<V> {
    [key: string]: V;
}

export namespace JsDictionary {
    export function isJsDictionary<V>(x: any): x is JsDictionary<V> {
        // We accept only literal objects and objects created via `new Object` as dictionaries.
        // Furthermore, we require no function-valued `__as_preserve__` property to exist.
        return typeof x === 'object'
            && x !== null
            && Object.getPrototypeOf(Object.getPrototypeOf(x)) === null
            && typeof x.__as_preserve__ !== 'function'
            && !isEmbedded(x);
    }

    export function from<V>(entries: Iterable<[symbol, V]>): JsDictionary<V> {
        const r: JsDictionary<V> = {};
        for (const [key, value] of entries) r[key.description!] = value;
        return r;
    }

    export function clear<V>(j: JsDictionary<V>): void {
        for (const key in j) delete j[key];
    }

    export function remove<V>(j: JsDictionary<V>, key: symbol): boolean {
        const result = has(j, key);
        delete j[key.description!];
        return result;
    }

    export function forEach<V>(
        j: JsDictionary<V>,
        callbackfn: (value: V, key: symbol) => void,
    ): void {
        Object.entries(j).forEach(([key, val]) => callbackfn(val, Symbol.for(key)));
    }

    export function get<V>(j: JsDictionary<V>, key: symbol): V | undefined {
        return j[key.description!];
    }

    export function has<V>(j: JsDictionary<V>, key: symbol): boolean {
        return Object.hasOwnProperty.call(j, key.description!);
    }

    export function set<V>(j: JsDictionary<V>, key: symbol, value: V): JsDictionary<V> {
        j[key.description!] = value;
        return j;
    }

    export function size<V>(j: JsDictionary<V>): number {
        return Object.keys(j).length;
    }

    export function entries<V>(j: JsDictionary<V>): IterableIterator<[symbol, V]> {
        return _iterMap(Object.entries(j).values(), ([k, v]) => [Symbol.for(k), v]);
    }

    export function keys<V>(j: JsDictionary<V>): IterableIterator<symbol> {
        return _iterMap(Object.keys(j).values(), k => Symbol.for(k));
    }

    export function values<V>(j: JsDictionary<V>): IterableIterator<V> {
        return Object.values(j).values();
    }

    export function clone<V>(j: JsDictionary<V>): JsDictionary<V> {
        const r: JsDictionary<V> = {};
        Object.keys(j).forEach(k => r[k] = j[k]);
        return r;
    }

    export function equals<V>(
        j1: JsDictionary<V>,
        j2: JsDictionary<V>,
        eqv: Equivalence<V> = (v1, v2) => v1 === v2,
    ): boolean {
        if (size(j1) !== size(j2)) return false;
        for (let [k, v] of entries(j1)) {
            if (!has(j2, k)) return false;
            if (!eqv(v, get(j2, k)!)) return false;
        }
        return true;
    }
}
