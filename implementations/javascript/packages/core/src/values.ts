// Preserves Values.

import type { Bytes } from './bytes';
import type { DoubleFloat } from './float';
import type { Annotated } from './annotated';
import type { JsDictionary } from './jsdictionary';
import { Set, KeyedDictionary } from './dictionary';
import type { Embeddable, GenericEmbedded } from './embedded';

export type Value<T extends Embeddable = GenericEmbedded> =
    | Atom
    | Compound<T>
    | T
    | Annotated<T>;
export type Atom =
    | boolean
    | DoubleFloat
    | number | bigint
    | string
    | Bytes
    | symbol;
export type Compound<T extends Embeddable = GenericEmbedded> =
    | (Array<Value<T>> | [Value<T>]) & { label: Value<T> }
      // ^ expanded from definition of Record<> in record.ts,
      // because if we use Record<Value<T>, Tuple<Value<T>>, T>,
      // TypeScript currently complains about circular use of Value<T>,
      // and if we use Record<any, any, T>, it accepts it but collapses
      // Value<T> to any.
    | Array<Value<T>>
    | Set<T>
      // v Expanded from definition of Dictionary<> in dictionary.ts,
      // because of circular-use-of-Value<T> issues.
    | JsDictionary<Value<T>>
    | KeyedDictionary<T>;
