import { Record, Tuple } from "./record";
import { Bytes } from "./bytes";
import { fold } from "./fold";
import { is } from "./is";
import { Value } from "./values";
import { Set, Dictionary, KeyedDictionary, DictionaryMap } from "./dictionary";
import { Annotated } from "./annotated";
import { unannotate } from "./strip";
import { isEmbedded } from "./embedded";
import { isCompound } from "./compound";
import type { Embeddable } from "./embedded";
import { JsDictionary } from "./jsdictionary";

export function merge<T extends Embeddable>(
    mergeEmbeddeds: (a: T, b: T) => T | undefined,
    item0: Value<T>,
    ... items: Array<Value<T>>): Value<T>
{
    function die(): never {
        throw new Error("Cannot merge items");
    }

    function walk(a: Value<T>, b: Value<T>): Value<T> {
        if (a === b) {
            // Shortcut for merges of trivially identical values.
            return a;
        }
        if (!isCompound(a) && !isCompound(b)) {
            // Don't do expensive recursive comparisons for compounds.
            if (is(a, b)) {
                // Shortcut for merges of marginally less trivially identical values.
                return a;
            }
        }
        return fold<T, Value<T>>(a, {
            boolean: die,
            double(_f: number) { return is(a, b) ? a : die(); },
            integer: die,
            string: die,
            bytes(_b: Bytes) { return is(a, b) ? a : die(); },
            symbol: die,

            record(r: Record<Value<T>, Tuple<Value<T>>, T>) {
                if (!Record.isRecord<Value<T>, Tuple<Value<T>>, T>(b)) die();
                return Record(walk(r.label, b.label), walkMany(r, b));
            },

            array(a: Array<Value<T>>) {
                if (!Array.isArray(b) || Record.isRecord(b)) die();
                return walkMany(a, b);
            },

            set(_s: Set<T>) { die(); },

            dictionary(aMap: DictionaryMap<T>) {
                const bMap = Dictionary.asMap<T>(b);
                if (bMap === void 0) die();

                const r = new DictionaryMap<T>();
                aMap.forEach((av,ak) => {
                    const bv = bMap.get(ak);
                    r.set(ak, bv === void 0 ? av : walk(av, bv));
                });
                bMap.forEach((bv, bk) => {
                    if (!aMap.has(bk)) r.set(bk, bv);
                });
                return r.simplifiedValue();
            },

            annotated(a: Annotated<T>) {
                return walk(a, unannotate(b));
            },

            embedded(t: T) {
                if (!isEmbedded<T>(b)) die();
                const r = mergeEmbeddeds(t, b);
                if (r === void 0) die();
                return r;
            },
        });
    }

    function walkMany(a: Array<Value<T>>, b: Array<Value<T>>): Array<Value<T>> {
        if (a.length <= b.length) {
            return b.map((bb, i) => (i < a.length) ? walk(a[i], bb) : bb);
        } else {
            return a.map((aa, i) => (i < b.length) ? walk(aa, b[i]) : aa);
        }
    }

    return items.reduce(walk, item0);
}
