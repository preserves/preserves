// Preserves-Expressions. https://preserves.dev/preserves-expressions.html

import { ReaderBase } from './reader';
import { Atom, Value } from './values';
import { Position, annotate, formatPosition } from './annotated';
import { Record as VRecord } from './record';
import { Embeddable, GenericEmbedded } from './embedded';
import { fromJS } from './fromjs';
import { DictionaryMap, Set as VSet } from './dictionary';

export type Expr = SimpleExpr | Punct;
export type SimpleExpr = Atom | Compound | Embedded;

export type Positioned<I> = { position: Position, item: I, annotations?: Annotations };

export class Punct {
    constructor(public text: string) {}
    __as_preserve__(): Value { return VRecord(Symbol.for('p'), [Symbol.for(this.text)]); }

    isComma(): boolean { return this.text === ','; }
    static isComma(v: Expr): boolean { return v instanceof Punct && v.isComma(); }

    isColon(n = 1): boolean { return this.text === ':'.repeat(n); }
    static isColon(v: Expr, n = 1): boolean { return v instanceof Punct && v.isColon(n); }
}

export class Embedded {
    constructor(public expr: SimpleExpr, public annotations?: Annotations) {}
    __as_preserve__(): Value {
        const v = fromJS(this.expr);
        return new GenericEmbedded(this.annotations?.wrap(v) ?? v);
    }
}

export class BaseCompound<I> {
    positions: Position[] = [];
    exprs: I[] = [];
    annotations?: Annotations[] = void 0; // sparse array when non-void

    get(i: number): Positioned<I> | undefined {
        if (i >= this.exprs.length) return void 0;
        return {
            position: this.positions[i],
            item: this.exprs[i],
            annotations: this.annotations && this.annotations[i],
        };
    }

    push(p: Positioned<I>): true;
    push(expr: I, position: Position, annotations?: Annotations): true;
    push(v: Positioned<I> | I, position?: Position, annotations?: Annotations) {
        if (position === void 0) {
            const p = v as Positioned<I>;
            if (p.annotations) this._ensureAnnotations()[this.exprs.length] = p.annotations;
            this.positions.push(p.position);
            this.exprs.push(p.item);
        } else {
            if (annotations) this._ensureAnnotations()[this.exprs.length] = annotations;
            this.positions.push(position);
            this.exprs.push(v as I);
        }
        return true;
    }

    _ensureAnnotations(): Annotations[] {
        if (this.annotations === void 0) this.annotations = [];
        return this.annotations;
    }

    _annotationsAt(index: number): Annotations {
        return this._ensureAnnotations()[index] ??= new Annotations();
    }

    preservesValues(): Value[] {
        return this.exprs.map((p, i) => {
            const v = fromJS(p);
            if (this.annotations?.[i] !== void 0) {
                return this.annotations[i].wrap(v);
            } else {
                return v;
            }
        });
    }

    __as_preserve__(): Value {
        return this.preservesValues();
    }

    map<R>(f: (item: Positioned<I>, index: number) => R, offset = 0): R[] {
        const result: R[] = [];
        for (let i = offset; i < this.exprs.length; i++) {
            result.push(f(this.get(i)!, i));
        }
        return result;
    }

    [Symbol.iterator](): IterableIterator<Positioned<I>> {
        let c = this;
        let i = 0;
        return {
            next(): IteratorResult<Positioned<I>> {
                if (i < c.exprs.length) {
                    return { done: false, value: c.get(i++)! };
                } else {
                    return { done: true, value: void 0 };
                }
            },
            [Symbol.iterator]() { return c[Symbol.iterator](); }
        };
    }
}

export class Document extends BaseCompound<Expr> {}

export class Annotations extends BaseCompound<SimpleExpr> {
    wrap(v: Value): Value {
        return annotate(v, ... this.preservesValues());
    }
}

export type CompoundVariant = 'sequence' | 'record' | 'block' | 'group' | 'set';

export abstract class Compound extends BaseCompound<Expr> {
    abstract get variant(): CompoundVariant;
    __as_preserve__(): Value {
        const vs = this.preservesValues();
        switch (this.variant) {
            case 'sequence': return vs;
            case 'record': return VRecord(Symbol.for('r'), vs);
            case 'block': return VRecord(Symbol.for('b'), vs);
            case 'group': return VRecord(Symbol.for('g'), vs);
            case 'set': return VRecord(Symbol.for('s'), vs);
        }
    }
}

export class Sequence extends Compound {
    get variant(): CompoundVariant { return 'sequence'; }
}

export class Record extends Compound {
    get variant(): CompoundVariant { return 'record'; }
}

export class Block extends Compound {
    get variant(): CompoundVariant { return 'block'; }
}

export class Group extends Compound {
    get variant(): CompoundVariant { return 'group'; }
}

export class Set extends Compound {
    get variant(): CompoundVariant { return 'set'; }
}

export class Reader extends ReaderBase<never> {
    nextDocument(howMany: 'all' | 'one' = 'all'): Document {
        const doc = new Document();
        this.readExpr(doc);
        if (howMany === 'all') {
            while (this.readExpr(doc)) {}
        }
        return doc;
    }

    readCompound(c: Compound, terminator: string): Compound {
        while (this.readExpr(c, terminator)) {}
        return c;
    }

    readSimpleExpr(c: BaseCompound<SimpleExpr>): boolean {
        return this._readInto(c, false);
    }

    readExpr(c: BaseCompound<Expr>, terminator: string | null = null): boolean {
        return this._readInto(c as BaseCompound<SimpleExpr> /* yuck */, true, terminator);
    }

    _checkTerminator(actual: string, expected: string | null, startPos: Position): false {
        if (actual === expected) return false;
        this.state.error('Unexpected ' + actual, startPos);
    }

    _readInto(c: BaseCompound<SimpleExpr>, acceptPunct: boolean, terminator: string | null = null): boolean {
        while (true) {
            this.state.skipws();
            if (this.state.atEnd() && terminator === null) return false;
            const startPos = this.state.copyPos();
            const ch = this.state.nextchar();
            switch (ch) {
                case '"':
                    return c.push(this.state.readString('"'), startPos);
                case "'":
                    return c.push(Symbol.for(this.state.readString("'")), startPos);
                case ';':
                    if (acceptPunct) {
                        return (c as BaseCompound<Expr>).push(new Punct(';'), startPos);
                    } else {
                        this.state.error('Semicolon is not permitted at this location', startPos);
                    }
                case '@':
                    if (!this.readSimpleExpr(c._annotationsAt(c.exprs.length))) {
                        this.state.error('Missing annotation', startPos);
                    }
                    continue;
                case ':': {
                    let colons: string = ch;
                    while (!this.state.atEnd() && this.state.peek() === ':') {
                        colons = colons + ':';
                        this.state.advance();
                    }
                    if (acceptPunct) {
                        return (c as BaseCompound<Expr>).push(new Punct(colons), startPos);
                    } else {
                        this.state.error('Colons are not permitted at this location', startPos);
                    }
                }
                case '#': {
                    const ch = this.state.nextchar();
                    switch (ch) {
                        case ' ': case '\t': {
                            const here = this.state.copyPos();
                            c._annotationsAt(c.exprs.length).push(this.state.readCommentLine(), here);
                            continue;
                        }
                        case '\n': case '\r': {
                            const here = this.state.copyPos();
                            c._annotationsAt(c.exprs.length).push('', here);
                            continue;
                        }
                        case '!': {
                            const here = this.state.copyPos();
                            const r = new Record();
                            r.push(Symbol.for('interpreter'), here);
                            r.push(this.state.readCommentLine(), here);
                            c._annotationsAt(c.exprs.length).push(r, here);
                            continue;
                        }
                        case 'f': this.state.requireDelimiter('#f'); return c.push(false, startPos);
                        case 't': this.state.requireDelimiter('#t'); return c.push(true, startPos);
                        case '{': return c.push(this.readCompound(new Set(), '}'), startPos);
                        case '"': return c.push(this.state.readLiteralBinary(), startPos);
                        case 'x': switch (this.state.nextchar()) {
                            case '"': return c.push(this.state.readHexBinary(), startPos);
                            case 'd': return c.push(this.state.readHexFloat(), startPos);
                            default: this.state.error('Invalid #x syntax', startPos);
                        }
                        case '[': return c.push(this.state.readBase64Binary(), startPos);
                        case ':': {
                            const r = new BaseCompound<SimpleExpr>();
                            if (!this.readSimpleExpr(r)) return false;
                            const e = new Embedded(r.exprs[0], r.annotations && r.annotations[0]);
                            return c.push(e, startPos);
                        }                            
                        default:
                            this.state.error(`Invalid # syntax: ${ch}`, startPos);
                    }
                }
                case '(': return c.push(this.readCompound(new Group(), ')'), startPos);
                case '<': return c.push(this.readCompound(new Record(), '>'), startPos);
                case '[': return c.push(this.readCompound(new Sequence(), ']'), startPos);
                case '{': return c.push(this.readCompound(new Block(), '}'), startPos);
                case ')': return this._checkTerminator(ch, terminator, startPos);
                case '>': return this._checkTerminator(ch, terminator, startPos);
                case ']': return this._checkTerminator(ch, terminator, startPos);
                case '}': return this._checkTerminator(ch, terminator, startPos);
                case ',':
                    if (acceptPunct) {
                        return (c as BaseCompound<Expr>).push(new Punct(','), startPos);
                    } else {
                        this.state.error('Comma is not permitted at this location', startPos);
                    }
                default:
                    return c.push(this.state.readRawSymbolOrNumber(ch), startPos);
            }
        }
    }
}

export interface AsPreservesOptions<T extends Embeddable> {
    onGroup?: (g: Positioned<Group>) => Value<T>;
    onEmbedded?: (e: Positioned<Expr>, walk: (p: Positioned<Expr>) => Value<T>) => Value<T>;
    error?: (tag: string, position: Position) => Value<T>;
}

export function asPreserves<T extends Embeddable>(
    p: Positioned<Expr>,
    options: AsPreservesOptions<T> = {},
): Value<T> {
    const error = options.error ?? ((tag, position) => {
        throw new Error(formatPosition(position) + ": " + tag);
    });

    function nonCommas(p: Compound): Positioned<Expr>[] {
        return Array.from(p).filter(p => !Punct.isComma(p.item));
    }

    function walk(p: Positioned<Expr>): Value<T> {
        if (p.item instanceof Punct) {
            return error('invalid-punctuation', p.position);
        } else if (p.item instanceof Embedded) {
            if (options.onEmbedded) {
                return options.onEmbedded({ position: p.position, item: p.item.expr }, walk);
            } else {
                return error('unexpected-embedded', p.position);
            }
        } else if (p.item instanceof Compound) {
            switch (p.item.variant) {
                case 'sequence':
                    return nonCommas(p.item).map(walk);
                case 'record': {
                    const vs = nonCommas(p.item).map(walk);
                    if (vs.length < 1) {
                        return error('invalid-record', p.position);
                    }
                    const r = vs.slice(1) as unknown as VRecord<Value<T>, Value<T>[], T>;
                    r.label = vs[0];
                    return r;
                }
                case 'block': {
                    const d = new DictionaryMap<T>();
                    const vs = nonCommas(p.item);
                    if ((vs.length % 3) !== 0) {
                        return error('invalid-dictionary', p.position);
                    }
                    for (let i = 0; i < vs.length; i += 3) {
                        if (!Punct.isColon(vs[i + 1].item)) {
                            return error('missing-colon', vs[i + 1].position);
                        }
                        const k = walk(vs[i]);
                        const v = walk(vs[i + 2]);
                        d.set(k, v);
                    }
                    return d.simplifiedValue();
                }
                case 'group': {
                    if (options.onGroup) {
                        return options.onGroup(p as Positioned<Group>);
                    } else {
                        return error('unexpected-group', p.position);
                    }
                }
                case 'set':
                    return new VSet(nonCommas(p.item).map(walk));
            }
        } else {
            return p.item;
        }
    }

    return walk(p);
}
