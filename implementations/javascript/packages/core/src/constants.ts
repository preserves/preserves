export enum Tag {
    False = 0x80,
    True,
    End = 0x84,
    Annotation,
    Embedded,
    Ieee754,

    SignedInteger = 0xb0,
    String,
    ByteString,
    Symbol,
    Record,
    Sequence,
    Set,
    Dictionary,
}
