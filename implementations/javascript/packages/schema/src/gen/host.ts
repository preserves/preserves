import * as _ from "@preserves/core";
import * as _i_schema from "./schema";

export const $any = _.Symbol.for("any");
export const $array = _.Symbol.for("array");
export const $embedded = _.Symbol.for("embedded");
export const $map = _.Symbol.for("map");
export const $rec = _.Symbol.for("rec");
export const $ref = _.Symbol.for("ref");
export const $set = _.Symbol.for("set");
export const $union = _.Symbol.for("union");
export const $unit = _.Symbol.for("unit");

let __schema: _.Value | null = null;

export function _schema() {
    if (__schema === null) {
        __schema = _.decode<_.GenericEmbedded>(_.Bytes.fromHex("b4b306736368656d61b7b30776657273696f6eb00101b30b646566696e6974696f6e73b7b3054669656c64b4b3026f72b5b5b104756e6974b4b3036c6974b304756e69748484b5b103616e79b4b3036c6974b303616e798484b5b108656d626564646564b4b3036c6974b308656d6265646465648484b5b1056172726179b4b303726563b4b3036c6974b305617272617984b4b3057475706c65b5b4b3056e616d6564b307656c656d656e74b4b303726566b584b3054669656c64848484848484b5b103736574b4b303726563b4b3036c6974b30373657484b4b3057475706c65b5b4b3056e616d6564b307656c656d656e74b4b303726566b584b3054669656c64848484848484b5b1036d6170b4b303726563b4b3036c6974b3036d617084b4b3057475706c65b5b4b3056e616d6564b3036b6579b4b303726566b584b3054669656c648484b4b3056e616d6564b30576616c7565b4b303726566b584b3054669656c64848484848484b5b103726566b4b303726563b4b3036c6974b30372656684b4b3057475706c65b5b4b3056e616d6564b3046e616d65b4b303726566b5b306736368656d6184b303526566848484848484b5b10841746f6d4b696e64b4b303726566b5b306736368656d6184b30841746f6d4b696e6484848484b3065265636f7264b4b303726563b4b3036c6974b30372656384b4b3057475706c65b5b4b3056e616d6564b3066669656c6473b4b3057365716f66b4b303726566b584b30a4e616d65644669656c64848484848484b30653696d706c65b4b3026f72b5b5b1054669656c64b4b303726566b584b3054669656c648484b5b1065265636f7264b4b303726566b584b3065265636f726484848484b30756617269616e74b4b3057475706c65b5b4b3056e616d6564b3056c6162656cb4b30461746f6db30653796d626f6c8484b4b3056e616d6564b30474797065b4b303726566b584b30653696d706c6584848484b30a446566696e6974696f6eb4b3026f72b5b5b105756e696f6eb4b303726563b4b3036c6974b305756e696f6e84b4b3057475706c65b5b4b3056e616d6564b30876617269616e7473b4b3057365716f66b4b303726566b584b30756617269616e7484848484848484b5b10653696d706c65b4b303726566b584b30653696d706c6584848484b30a4e616d65644669656c64b4b3057475706c65b5b4b3056e616d6564b3046e616d65b4b30461746f6db30653796d626f6c8484b4b3056e616d6564b30474797065b4b303726566b584b3054669656c648484848484b30c656d62656464656454797065808484"));
    };
    return __schema;
}

export const _imports = {"schema": _i_schema}


export type Definition = (
    (
        {"_variant": "union", "variants": Array<Variant>} |
        {"_variant": "Simple", "value": Simple}
    ) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Variant = (
    {"label": symbol, "type": Simple} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Simple = (
    (
        {"_variant": "Field", "value": Field} |
        {"_variant": "Record", "value": Record}
    ) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Record = (
    {"fields": Array<NamedField>} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type NamedField = (
    {"name": symbol, "type": Field} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Field = (
    (
        {"_variant": "unit"} |
        {"_variant": "any"} |
        {"_variant": "embedded"} |
        {"_variant": "array", "element": Field} |
        {"_variant": "set", "element": Field} |
        {"_variant": "map", "key": Field, "value": Field} |
        {"_variant": "ref", "name": _i_schema.Ref} |
        {"_variant": "AtomKind", "value": _i_schema.AtomKind}
    ) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);


export namespace Definition {
    export function union(variants: Array<Variant>): Definition {
        return {
            "_variant": "union",
            "variants": variants,
            __as_preserve__() {return fromDefinition(this);},
            __preserve_on__(e) { e.push(fromDefinition(this)); },
            __preserve_text_on__(w) { w.push(fromDefinition(this)); }
        };
    };
    union.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Definition"),
            variant: _.Symbol.for("union")
        };
    };
    export function Simple(value: Simple): Definition {
        return {
            "_variant": "Simple",
            "value": value,
            __as_preserve__() {return fromDefinition(this);},
            __preserve_on__(e) { e.push(fromDefinition(this)); },
            __preserve_text_on__(w) { w.push(fromDefinition(this)); }
        };
    };
    Simple.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Definition"),
            variant: _.Symbol.for("Simple")
        };
    };
}

export function Variant({label, type}: {label: symbol, type: Simple}): Variant {
    return {
        "label": label,
        "type": type,
        __as_preserve__() {return fromVariant(this);},
        __preserve_on__(e) { e.push(fromVariant(this)); },
        __preserve_text_on__(w) { w.push(fromVariant(this)); }
    };
}

Variant.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Variant")};
}

export namespace Simple {
    export function Field(value: Field): Simple {
        return {
            "_variant": "Field",
            "value": value,
            __as_preserve__() {return fromSimple(this);},
            __preserve_on__(e) { e.push(fromSimple(this)); },
            __preserve_text_on__(w) { w.push(fromSimple(this)); }
        };
    };
    Field.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Simple"),
            variant: _.Symbol.for("Field")
        };
    };
    export function Record(value: Record): Simple {
        return {
            "_variant": "Record",
            "value": value,
            __as_preserve__() {return fromSimple(this);},
            __preserve_on__(e) { e.push(fromSimple(this)); },
            __preserve_text_on__(w) { w.push(fromSimple(this)); }
        };
    };
    Record.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Simple"),
            variant: _.Symbol.for("Record")
        };
    };
}

export function Record(fields: Array<NamedField>): Record {
    return {
        "fields": fields,
        __as_preserve__() {return fromRecord(this);},
        __preserve_on__(e) { e.push(fromRecord(this)); },
        __preserve_text_on__(w) { w.push(fromRecord(this)); }
    };
}

Record.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Record")};
}

export function NamedField({name, type}: {name: symbol, type: Field}): NamedField {
    return {
        "name": name,
        "type": type,
        __as_preserve__() {return fromNamedField(this);},
        __preserve_on__(e) { e.push(fromNamedField(this)); },
        __preserve_text_on__(w) { w.push(fromNamedField(this)); }
    };
}

NamedField.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("NamedField")
    };
}

export namespace Field {
    export function unit(): Field {
        return {
            "_variant": "unit",
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    unit.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("unit")
        };
    };
    export function any(): Field {
        return {
            "_variant": "any",
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    any.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("any")
        };
    };
    export function embedded(): Field {
        return {
            "_variant": "embedded",
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    embedded.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("embedded")
        };
    };
    export function array(element: Field): Field {
        return {
            "_variant": "array",
            "element": element,
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    array.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("array")
        };
    };
    export function set(element: Field): Field {
        return {
            "_variant": "set",
            "element": element,
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    set.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("set")
        };
    };
    export function map({key, value}: {key: Field, value: Field}): Field {
        return {
            "_variant": "map",
            "key": key,
            "value": value,
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    map.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("map")
        };
    };
    export function ref(name: _i_schema.Ref): Field {
        return {
            "_variant": "ref",
            "name": name,
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    ref.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("ref")
        };
    };
    export function AtomKind(value: _i_schema.AtomKind): Field {
        return {
            "_variant": "AtomKind",
            "value": value,
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    AtomKind.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Field"),
            variant: _.Symbol.for("AtomKind")
        };
    };
}

export function asDefinition<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Definition {
    let result = toDefinition(v);
    if (result === void 0) throw new TypeError(`Invalid Definition: ${_.stringify(v)}`);
    return result;
}

export function toDefinition<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Definition {
    let result: undefined | Definition;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $union) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Array<Variant>) | undefined;
            _tmp1 = void 0;
            if (_.isSequence(v[0])) {
                _tmp1 = [];
                for (const _tmp2 of v[0]) {
                    let _tmp3: (Variant) | undefined;
                    _tmp3 = toVariant(_tmp2);
                    if (_tmp3 !== void 0) {_tmp1.push(_tmp3); continue;};
                    _tmp1 = void 0;
                    break;
                };
            };
            if (_tmp1 !== void 0) {
                result = {
                    "_variant": "union",
                    "variants": _tmp1,
                    __as_preserve__() {return fromDefinition(this);},
                    __preserve_on__(e) { e.push(fromDefinition(this)); },
                    __preserve_text_on__(w) { w.push(fromDefinition(this)); }
                };
            };
        };
    };
    if (result === void 0) {
        let _tmp4: (Simple) | undefined;
        _tmp4 = toSimple(v);
        if (_tmp4 !== void 0) {
            result = {
                "_variant": "Simple",
                "value": _tmp4,
                __as_preserve__() {return fromDefinition(this);},
                __preserve_on__(e) { e.push(fromDefinition(this)); },
                __preserve_text_on__(w) { w.push(fromDefinition(this)); }
            };
        };
    };
    return result;
}

export namespace Definition {export const __from_preserve__ = toDefinition;}

export function fromDefinition<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Definition): _.Value<_embedded> {
    switch (_v._variant) {
        case "union": {return _.Record($union, [_v["variants"].map(v => fromVariant<_embedded>(v))]);};
        case "Simple": {return fromSimple<_embedded>(_v.value);};
    };
}

export function asVariant<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Variant {
    let result = toVariant(v);
    if (result === void 0) throw new TypeError(`Invalid Variant: ${_.stringify(v)}`);
    return result;
}

export function toVariant<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Variant {
    let result: undefined | Variant;
    if (_.isSequence(v) && v.length >= 2) {
        let _tmp0: (symbol) | undefined;
        _tmp0 = typeof v[0] === 'symbol' ? v[0] : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Simple) | undefined;
            _tmp1 = toSimple(v[1]);
            if (_tmp1 !== void 0) {
                result = {
                    "label": _tmp0,
                    "type": _tmp1,
                    __as_preserve__() {return fromVariant(this);},
                    __preserve_on__(e) { e.push(fromVariant(this)); },
                    __preserve_text_on__(w) { w.push(fromVariant(this)); }
                };
            };
        };
    };
    return result;
}

Variant.__from_preserve__ = toVariant;

export function fromVariant<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Variant): _.Value<_embedded> {return [_v["label"], fromSimple<_embedded>(_v["type"])];}

export function asSimple<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Simple {
    let result = toSimple(v);
    if (result === void 0) throw new TypeError(`Invalid Simple: ${_.stringify(v)}`);
    return result;
}

export function toSimple<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Simple {
    let _tmp0: (Field) | undefined;
    let result: undefined | Simple;
    _tmp0 = toField(v);
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "Field",
            "value": _tmp0,
            __as_preserve__() {return fromSimple(this);},
            __preserve_on__(e) { e.push(fromSimple(this)); },
            __preserve_text_on__(w) { w.push(fromSimple(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: (Record) | undefined;
        _tmp1 = toRecord(v);
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "Record",
                "value": _tmp1,
                __as_preserve__() {return fromSimple(this);},
                __preserve_on__(e) { e.push(fromSimple(this)); },
                __preserve_text_on__(w) { w.push(fromSimple(this)); }
            };
        };
    };
    return result;
}

export namespace Simple {export const __from_preserve__ = toSimple;}

export function fromSimple<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Simple): _.Value<_embedded> {
    switch (_v._variant) {
        case "Field": {return fromField<_embedded>(_v.value);};
        case "Record": {return fromRecord<_embedded>(_v.value);};
    };
}

export function asRecord<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Record {
    let result = toRecord(v);
    if (result === void 0) throw new TypeError(`Invalid Record: ${_.stringify(v)}`);
    return result;
}

export function toRecord<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Record {
    let result: undefined | Record;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $rec) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Array<NamedField>) | undefined;
            _tmp1 = void 0;
            if (_.isSequence(v[0])) {
                _tmp1 = [];
                for (const _tmp2 of v[0]) {
                    let _tmp3: (NamedField) | undefined;
                    _tmp3 = toNamedField(_tmp2);
                    if (_tmp3 !== void 0) {_tmp1.push(_tmp3); continue;};
                    _tmp1 = void 0;
                    break;
                };
            };
            if (_tmp1 !== void 0) {
                result = {
                    "fields": _tmp1,
                    __as_preserve__() {return fromRecord(this);},
                    __preserve_on__(e) { e.push(fromRecord(this)); },
                    __preserve_text_on__(w) { w.push(fromRecord(this)); }
                };
            };
        };
    };
    return result;
}

Record.__from_preserve__ = toRecord;

export function fromRecord<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Record): _.Value<_embedded> {return _.Record($rec, [_v["fields"].map(v => fromNamedField<_embedded>(v))]);}

export function asNamedField<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): NamedField {
    let result = toNamedField(v);
    if (result === void 0) throw new TypeError(`Invalid NamedField: ${_.stringify(v)}`);
    return result;
}

export function toNamedField<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedField {
    let result: undefined | NamedField;
    if (_.isSequence(v) && v.length >= 2) {
        let _tmp0: (symbol) | undefined;
        _tmp0 = typeof v[0] === 'symbol' ? v[0] : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Field) | undefined;
            _tmp1 = toField(v[1]);
            if (_tmp1 !== void 0) {
                result = {
                    "name": _tmp0,
                    "type": _tmp1,
                    __as_preserve__() {return fromNamedField(this);},
                    __preserve_on__(e) { e.push(fromNamedField(this)); },
                    __preserve_text_on__(w) { w.push(fromNamedField(this)); }
                };
            };
        };
    };
    return result;
}

NamedField.__from_preserve__ = toNamedField;

export function fromNamedField<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: NamedField): _.Value<_embedded> {return [_v["name"], fromField<_embedded>(_v["type"])];}

export function asField<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Field {
    let result = toField(v);
    if (result === void 0) throw new TypeError(`Invalid Field: ${_.stringify(v)}`);
    return result;
}

export function toField<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Field {
    let _tmp0: ({}) | undefined;
    let result: undefined | Field;
    _tmp0 = _.is(v, $unit) ? {} : void 0;
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "unit",
            __as_preserve__() {return fromField(this);},
            __preserve_on__(e) { e.push(fromField(this)); },
            __preserve_text_on__(w) { w.push(fromField(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: ({}) | undefined;
        _tmp1 = _.is(v, $any) ? {} : void 0;
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "any",
                __as_preserve__() {return fromField(this);},
                __preserve_on__(e) { e.push(fromField(this)); },
                __preserve_text_on__(w) { w.push(fromField(this)); }
            };
        };
        if (result === void 0) {
            let _tmp2: ({}) | undefined;
            _tmp2 = _.is(v, $embedded) ? {} : void 0;
            if (_tmp2 !== void 0) {
                result = {
                    "_variant": "embedded",
                    __as_preserve__() {return fromField(this);},
                    __preserve_on__(e) { e.push(fromField(this)); },
                    __preserve_text_on__(w) { w.push(fromField(this)); }
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp3: ({}) | undefined;
                    _tmp3 = _.is(v.label, $array) ? {} : void 0;
                    if (_tmp3 !== void 0) {
                        let _tmp4: (Field) | undefined;
                        _tmp4 = toField(v[0]);
                        if (_tmp4 !== void 0) {
                            result = {
                                "_variant": "array",
                                "element": _tmp4,
                                __as_preserve__() {return fromField(this);},
                                __preserve_on__(e) { e.push(fromField(this)); },
                                __preserve_text_on__(w) { w.push(fromField(this)); }
                            };
                        };
                    };
                };
                if (result === void 0) {
                    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                        let _tmp5: ({}) | undefined;
                        _tmp5 = _.is(v.label, $set) ? {} : void 0;
                        if (_tmp5 !== void 0) {
                            let _tmp6: (Field) | undefined;
                            _tmp6 = toField(v[0]);
                            if (_tmp6 !== void 0) {
                                result = {
                                    "_variant": "set",
                                    "element": _tmp6,
                                    __as_preserve__() {return fromField(this);},
                                    __preserve_on__(e) { e.push(fromField(this)); },
                                    __preserve_text_on__(w) { w.push(fromField(this)); }
                                };
                            };
                        };
                    };
                    if (result === void 0) {
                        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                            let _tmp7: ({}) | undefined;
                            _tmp7 = _.is(v.label, $map) ? {} : void 0;
                            if (_tmp7 !== void 0) {
                                let _tmp8: (Field) | undefined;
                                _tmp8 = toField(v[0]);
                                if (_tmp8 !== void 0) {
                                    let _tmp9: (Field) | undefined;
                                    _tmp9 = toField(v[1]);
                                    if (_tmp9 !== void 0) {
                                        result = {
                                            "_variant": "map",
                                            "key": _tmp8,
                                            "value": _tmp9,
                                            __as_preserve__() {return fromField(this);},
                                            __preserve_on__(e) { e.push(fromField(this)); },
                                            __preserve_text_on__(w) { w.push(fromField(this)); }
                                        };
                                    };
                                };
                            };
                        };
                        if (result === void 0) {
                            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                                let _tmp10: ({}) | undefined;
                                _tmp10 = _.is(v.label, $ref) ? {} : void 0;
                                if (_tmp10 !== void 0) {
                                    let _tmp11: (_i_schema.Ref) | undefined;
                                    _tmp11 = _i_schema.toRef<_embedded>(v[0]);
                                    if (_tmp11 !== void 0) {
                                        result = {
                                            "_variant": "ref",
                                            "name": _tmp11,
                                            __as_preserve__() {return fromField(this);},
                                            __preserve_on__(e) { e.push(fromField(this)); },
                                            __preserve_text_on__(w) { w.push(fromField(this)); }
                                        };
                                    };
                                };
                            };
                            if (result === void 0) {
                                let _tmp12: (_i_schema.AtomKind) | undefined;
                                _tmp12 = _i_schema.toAtomKind<_embedded>(v);
                                if (_tmp12 !== void 0) {
                                    result = {
                                        "_variant": "AtomKind",
                                        "value": _tmp12,
                                        __as_preserve__() {return fromField(this);},
                                        __preserve_on__(e) { e.push(fromField(this)); },
                                        __preserve_text_on__(w) { w.push(fromField(this)); }
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export namespace Field {export const __from_preserve__ = toField;}

export function fromField<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Field): _.Value<_embedded> {
    switch (_v._variant) {
        case "unit": {return $unit;};
        case "any": {return $any;};
        case "embedded": {return $embedded;};
        case "array": {return _.Record($array, [fromField<_embedded>(_v["element"])]);};
        case "set": {return _.Record($set, [fromField<_embedded>(_v["element"])]);};
        case "map": {
            return _.Record($map, [fromField<_embedded>(_v["key"]), fromField<_embedded>(_v["value"])]);
        };
        case "ref": {return _.Record($ref, [_i_schema.fromRef<_embedded>(_v["name"])]);};
        case "AtomKind": {return _i_schema.fromAtomKind<_embedded>(_v.value);};
    };
}

