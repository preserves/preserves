import * as _ from "@preserves/core";

export const $1 = 1;
export const $Boolean = _.Symbol.for("Boolean");
export const $ByteString = _.Symbol.for("ByteString");
export const $Double = _.Symbol.for("Double");
export const $SignedInteger = _.Symbol.for("SignedInteger");
export const $String = _.Symbol.for("String");
export const $Symbol = _.Symbol.for("Symbol");
export const $and = _.Symbol.for("and");
export const $any = _.Symbol.for("any");
export const $atom = _.Symbol.for("atom");
export const $bundle = _.Symbol.for("bundle");
export const $definitions = _.Symbol.for("definitions");
export const $dict = _.Symbol.for("dict");
export const $dictof = _.Symbol.for("dictof");
export const $embedded = _.Symbol.for("embedded");
export const $embeddedType = _.Symbol.for("embeddedType");
export const $lit = _.Symbol.for("lit");
export const $named = _.Symbol.for("named");
export const $or = _.Symbol.for("or");
export const $rec = _.Symbol.for("rec");
export const $ref = _.Symbol.for("ref");
export const $schema = _.Symbol.for("schema");
export const $seqof = _.Symbol.for("seqof");
export const $setof = _.Symbol.for("setof");
export const $tuple = _.Symbol.for("tuple");
export const $tuplePrefix = _.Symbol.for("tuplePrefix");
export const $version = _.Symbol.for("version");
export const __lit6 = false;

let __schema: _.Value | null = null;

export function _schema() {
    if (__schema === null) {
        __schema = _.decode<_.GenericEmbedded>(_.Bytes.fromHex("b4b306736368656d61b7b30776657273696f6eb00101b30b646566696e6974696f6e73b7b303526566b4b303726563b4b3036c6974b30372656684b4b3057475706c65b5b4b3056e616d6564b3066d6f64756c65b4b303726566b584b30a4d6f64756c65506174688484b4b3056e616d6564b3046e616d65b4b30461746f6db30653796d626f6c8484848484b30642756e646c65b4b303726563b4b3036c6974b30662756e646c6584b4b3057475706c65b5b4b3056e616d6564b3076d6f64756c6573b4b303726566b584b3074d6f64756c65738484848484b306536368656d61b4b303726563b4b3036c6974b306736368656d6184b4b3057475706c65b5b4b30464696374b7b30776657273696f6eb4b3056e616d6564b30776657273696f6eb4b303726566b584b30756657273696f6e8484b30b646566696e6974696f6e73b4b3056e616d6564b30b646566696e6974696f6e73b4b303726566b584b30b446566696e6974696f6e738484b30c656d62656464656454797065b4b3056e616d6564b30c656d62656464656454797065b4b303726566b584b310456d626564646564547970654e616d6584848484848484b30742696e64696e67b4b303726563b4b3036c6974b3056e616d656484b4b3057475706c65b5b4b3056e616d6564b3046e616d65b4b30461746f6db30653796d626f6c8484b4b3056e616d6564b3077061747465726eb4b303726566b584b30d53696d706c655061747465726e8484848484b3074d6f64756c6573b4b306646963746f66b4b303726566b584b30a4d6f64756c655061746884b4b303726566b584b306536368656d618484b3075061747465726eb4b3026f72b5b5b10d53696d706c655061747465726eb4b303726566b584b30d53696d706c655061747465726e8484b5b10f436f6d706f756e645061747465726eb4b303726566b584b30f436f6d706f756e645061747465726e84848484b30756657273696f6eb4b3036c6974b0010184b30841746f6d4b696e64b4b3026f72b5b5b107426f6f6c65616eb4b3036c6974b307426f6f6c65616e8484b5b106446f75626c65b4b3036c6974b306446f75626c658484b5b10d5369676e6564496e7465676572b4b3036c6974b30d5369676e6564496e74656765728484b5b106537472696e67b4b3036c6974b306537472696e678484b5b10a42797465537472696e67b4b3036c6974b30a42797465537472696e678484b5b10653796d626f6cb4b3036c6974b30653796d626f6c84848484b30a446566696e6974696f6eb4b3026f72b5b5b1026f72b4b303726563b4b3036c6974b3026f7284b4b3057475706c65b5b4b30b7475706c65507265666978b5b4b3056e616d6564b3087061747465726e30b4b303726566b584b3104e616d6564416c7465726e61746976658484b4b3056e616d6564b3087061747465726e31b4b303726566b584b3104e616d6564416c7465726e6174697665848484b4b3056e616d6564b3087061747465726e4eb4b3057365716f66b4b303726566b584b3104e616d6564416c7465726e61746976658484848484848484b5b103616e64b4b303726563b4b3036c6974b303616e6484b4b3057475706c65b5b4b30b7475706c65507265666978b5b4b3056e616d6564b3087061747465726e30b4b303726566b584b30c4e616d65645061747465726e8484b4b3056e616d6564b3087061747465726e31b4b303726566b584b30c4e616d65645061747465726e848484b4b3056e616d6564b3087061747465726e4eb4b3057365716f66b4b303726566b584b30c4e616d65645061747465726e8484848484848484b5b1075061747465726eb4b303726566b584b3075061747465726e84848484b30a4d6f64756c6550617468b4b3057365716f66b4b30461746f6db30653796d626f6c8484b30b446566696e6974696f6e73b4b306646963746f66b4b30461746f6db30653796d626f6c84b4b303726566b584b30a446566696e6974696f6e8484b30c4e616d65645061747465726eb4b3026f72b5b5b1056e616d6564b4b303726566b584b30742696e64696e678484b5b109616e6f6e796d6f7573b4b303726566b584b3075061747465726e84848484b30d53696d706c655061747465726eb4b3026f72b5b5b103616e79b4b3036c6974b303616e798484b5b10461746f6db4b303726563b4b3036c6974b30461746f6d84b4b3057475706c65b5b4b3056e616d6564b30861746f6d4b696e64b4b303726566b584b30841746f6d4b696e64848484848484b5b108656d626564646564b4b303726563b4b3036c6974b308656d62656464656484b4b3057475706c65b5b4b3056e616d6564b309696e74657266616365b4b303726566b584b30d53696d706c655061747465726e848484848484b5b1036c6974b4b303726563b4b3036c6974b3036c697484b4b3057475706c65b5b4b3056e616d6564b30576616c7565b303616e798484848484b5b1057365716f66b4b303726563b4b3036c6974b3057365716f6684b4b3057475706c65b5b4b3056e616d6564b3077061747465726eb4b303726566b584b30d53696d706c655061747465726e848484848484b5b1057365746f66b4b303726563b4b3036c6974b3057365746f6684b4b3057475706c65b5b4b3056e616d6564b3077061747465726eb4b303726566b584b30d53696d706c655061747465726e848484848484b5b106646963746f66b4b303726563b4b3036c6974b306646963746f6684b4b3057475706c65b5b4b3056e616d6564b3036b6579b4b303726566b584b30d53696d706c655061747465726e8484b4b3056e616d6564b30576616c7565b4b303726566b584b30d53696d706c655061747465726e848484848484b5b103526566b4b303726566b584b30352656684848484b30f436f6d706f756e645061747465726eb4b3026f72b5b5b103726563b4b303726563b4b3036c6974b30372656384b4b3057475706c65b5b4b3056e616d6564b3056c6162656cb4b303726566b584b30c4e616d65645061747465726e8484b4b3056e616d6564b3066669656c6473b4b303726566b584b30c4e616d65645061747465726e848484848484b5b1057475706c65b4b303726563b4b3036c6974b3057475706c6584b4b3057475706c65b5b4b3056e616d6564b3087061747465726e73b4b3057365716f66b4b303726566b584b30c4e616d65645061747465726e84848484848484b5b10b7475706c65507265666978b4b303726563b4b3036c6974b30b7475706c6550726566697884b4b3057475706c65b5b4b3056e616d6564b3056669786564b4b3057365716f66b4b303726566b584b30c4e616d65645061747465726e848484b4b3056e616d6564b3087661726961626c65b4b303726566b584b3124e616d656453696d706c655061747465726e848484848484b5b10464696374b4b303726563b4b3036c6974b3046469637484b4b3057475706c65b5b4b3056e616d6564b307656e7472696573b4b303726566b584b31144696374696f6e617279456e74726965738484848484848484b310456d626564646564547970654e616d65b4b3026f72b5b5b10566616c7365b4b3036c6974808484b5b103526566b4b303726566b584b30352656684848484b3104e616d6564416c7465726e6174697665b4b3057475706c65b5b4b3056e616d6564b30c76617269616e744c6162656cb4b30461746f6db306537472696e678484b4b3056e616d6564b3077061747465726eb4b303726566b584b3075061747465726e84848484b31144696374696f6e617279456e7472696573b4b306646963746f66b303616e79b4b303726566b584b3124e616d656453696d706c655061747465726e8484b3124e616d656453696d706c655061747465726eb4b3026f72b5b5b1056e616d6564b4b303726566b584b30742696e64696e678484b5b109616e6f6e796d6f7573b4b303726566b584b30d53696d706c655061747465726e8484848484b30c656d62656464656454797065808484"));
    };
    return __schema;
}

export const _imports = {}


export type Bundle<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    {"modules": Modules<_embedded>} &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type Modules<_embedded extends _.Embeddable = _.GenericEmbedded> = _.EncodableDictionary<_embedded, ModulePath, Schema<_embedded>>;

export type Schema<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    {
        "version": Version,
        "embeddedType": EmbeddedTypeName,
        "definitions": Definitions<_embedded>
    } &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type Version = (
    {} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type EmbeddedTypeName = (
    ({"_variant": "false"} | {"_variant": "Ref", "value": Ref}) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Definitions<_embedded extends _.Embeddable = _.GenericEmbedded> = _.JsDictionary<Definition<_embedded>>;

export type Definition<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    (
        {
            "_variant": "or",
            "pattern0": NamedAlternative<_embedded>,
            "pattern1": NamedAlternative<_embedded>,
            "patternN": Array<NamedAlternative<_embedded>>
        } |
        {
            "_variant": "and",
            "pattern0": NamedPattern<_embedded>,
            "pattern1": NamedPattern<_embedded>,
            "patternN": Array<NamedPattern<_embedded>>
        } |
        {"_variant": "Pattern", "value": Pattern<_embedded>}
    ) &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type Pattern<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    (
        {"_variant": "SimplePattern", "value": SimplePattern<_embedded>} |
        {"_variant": "CompoundPattern", "value": CompoundPattern<_embedded>}
    ) &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type SimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    (
        {"_variant": "any"} |
        {"_variant": "atom", "atomKind": AtomKind} |
        {"_variant": "embedded", "interface": SimplePattern<_embedded>} |
        {"_variant": "lit", "value": _.Value<_embedded>} |
        {"_variant": "seqof", "pattern": SimplePattern<_embedded>} |
        {"_variant": "setof", "pattern": SimplePattern<_embedded>} |
        {
            "_variant": "dictof",
            "key": SimplePattern<_embedded>,
            "value": SimplePattern<_embedded>
        } |
        {"_variant": "Ref", "value": Ref}
    ) &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type CompoundPattern<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    (
        {
            "_variant": "rec",
            "label": NamedPattern<_embedded>,
            "fields": NamedPattern<_embedded>
        } |
        {"_variant": "tuple", "patterns": Array<NamedPattern<_embedded>>} |
        {
            "_variant": "tuplePrefix",
            "fixed": Array<NamedPattern<_embedded>>,
            "variable": NamedSimplePattern<_embedded>
        } |
        {"_variant": "dict", "entries": DictionaryEntries<_embedded>}
    ) &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type DictionaryEntries<_embedded extends _.Embeddable = _.GenericEmbedded> = _.EncodableDictionary<_embedded, _.Value<_embedded>, NamedSimplePattern<_embedded>>;

export type AtomKind = (
    (
        {"_variant": "Boolean"} |
        {"_variant": "Double"} |
        {"_variant": "SignedInteger"} |
        {"_variant": "String"} |
        {"_variant": "ByteString"} |
        {"_variant": "Symbol"}
    ) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type NamedAlternative<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    {"variantLabel": string, "pattern": Pattern<_embedded>} &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type NamedSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    (
        {"_variant": "named", "value": Binding<_embedded>} |
        {"_variant": "anonymous", "value": SimplePattern<_embedded>}
    ) &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type NamedPattern<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    (
        {"_variant": "named", "value": Binding<_embedded>} |
        {"_variant": "anonymous", "value": Pattern<_embedded>}
    ) &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type Binding<_embedded extends _.Embeddable = _.GenericEmbedded> = (
    {"name": symbol, "pattern": SimplePattern<_embedded>} &
    _.Preservable<_embedded> &
    _.PreserveWritable<_embedded> &
    {__as_preserve__(): _.Value<_embedded>}
);

export type Ref = (
    {"module": ModulePath, "name": symbol} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type ModulePath = Array<symbol>;


export function Bundle<_embedded extends _.Embeddable = _.GenericEmbedded>(modules: Modules<_embedded>): Bundle<_embedded> {
    return {
        "modules": modules,
        __as_preserve__() {return fromBundle(this);},
        __preserve_on__(e) { e.push(fromBundle(this)); },
        __preserve_text_on__(w) { w.push(fromBundle(this)); }
    };
}

Bundle.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Bundle")};
}

export function Modules<_embedded extends _.Embeddable = _.GenericEmbedded>(value: _.EncodableDictionary<_embedded, ModulePath, Schema<_embedded>>): Modules<_embedded> {return value;}

Modules.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Modules")};
}

export function Schema<_embedded extends _.Embeddable = _.GenericEmbedded>(
    {version, embeddedType, definitions}: {
        version: Version,
        embeddedType: EmbeddedTypeName,
        definitions: Definitions<_embedded>
    }
): Schema<_embedded> {
    return {
        "version": version,
        "embeddedType": embeddedType,
        "definitions": definitions,
        __as_preserve__() {return fromSchema(this);},
        __preserve_on__(e) { e.push(fromSchema(this)); },
        __preserve_text_on__(w) { w.push(fromSchema(this)); }
    };
}

Schema.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Schema")};
}

export function Version(): Version {
    return {
        __as_preserve__() {return fromVersion(this);},
        __preserve_on__(e) { e.push(fromVersion(this)); },
        __preserve_text_on__(w) { w.push(fromVersion(this)); }
    };
}

Version.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Version")};
}

export namespace EmbeddedTypeName {
    export function $false(): EmbeddedTypeName {
        return {
            "_variant": "false",
            __as_preserve__() {return fromEmbeddedTypeName(this);},
            __preserve_on__(e) { e.push(fromEmbeddedTypeName(this)); },
            __preserve_text_on__(w) { w.push(fromEmbeddedTypeName(this)); }
        };
    };
    $false.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("EmbeddedTypeName"),
            variant: _.Symbol.for("false")
        };
    };
    export function Ref(value: Ref): EmbeddedTypeName {
        return {
            "_variant": "Ref",
            "value": value,
            __as_preserve__() {return fromEmbeddedTypeName(this);},
            __preserve_on__(e) { e.push(fromEmbeddedTypeName(this)); },
            __preserve_text_on__(w) { w.push(fromEmbeddedTypeName(this)); }
        };
    };
    Ref.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("EmbeddedTypeName"),
            variant: _.Symbol.for("Ref")
        };
    };
}

export function Definitions<_embedded extends _.Embeddable = _.GenericEmbedded>(value: _.JsDictionary<Definition<_embedded>>): Definitions<_embedded> {return value;}

Definitions.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("Definitions")
    };
}

export namespace Definition {
    export function or<_embedded extends _.Embeddable = _.GenericEmbedded>(
        {pattern0, pattern1, patternN}: {
            pattern0: NamedAlternative<_embedded>,
            pattern1: NamedAlternative<_embedded>,
            patternN: Array<NamedAlternative<_embedded>>
        }
    ): Definition<_embedded> {
        return {
            "_variant": "or",
            "pattern0": pattern0,
            "pattern1": pattern1,
            "patternN": patternN,
            __as_preserve__() {return fromDefinition(this);},
            __preserve_on__(e) { e.push(fromDefinition(this)); },
            __preserve_text_on__(w) { w.push(fromDefinition(this)); }
        };
    };
    or.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Definition"),
            variant: _.Symbol.for("or")
        };
    };
    export function and<_embedded extends _.Embeddable = _.GenericEmbedded>(
        {pattern0, pattern1, patternN}: {
            pattern0: NamedPattern<_embedded>,
            pattern1: NamedPattern<_embedded>,
            patternN: Array<NamedPattern<_embedded>>
        }
    ): Definition<_embedded> {
        return {
            "_variant": "and",
            "pattern0": pattern0,
            "pattern1": pattern1,
            "patternN": patternN,
            __as_preserve__() {return fromDefinition(this);},
            __preserve_on__(e) { e.push(fromDefinition(this)); },
            __preserve_text_on__(w) { w.push(fromDefinition(this)); }
        };
    };
    and.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Definition"),
            variant: _.Symbol.for("and")
        };
    };
    export function Pattern<_embedded extends _.Embeddable = _.GenericEmbedded>(value: Pattern<_embedded>): Definition<_embedded> {
        return {
            "_variant": "Pattern",
            "value": value,
            __as_preserve__() {return fromDefinition(this);},
            __preserve_on__(e) { e.push(fromDefinition(this)); },
            __preserve_text_on__(w) { w.push(fromDefinition(this)); }
        };
    };
    Pattern.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Definition"),
            variant: _.Symbol.for("Pattern")
        };
    };
}

export namespace Pattern {
    export function SimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(value: SimplePattern<_embedded>): Pattern<_embedded> {
        return {
            "_variant": "SimplePattern",
            "value": value,
            __as_preserve__() {return fromPattern(this);},
            __preserve_on__(e) { e.push(fromPattern(this)); },
            __preserve_text_on__(w) { w.push(fromPattern(this)); }
        };
    };
    SimplePattern.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Pattern"),
            variant: _.Symbol.for("SimplePattern")
        };
    };
    export function CompoundPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(value: CompoundPattern<_embedded>): Pattern<_embedded> {
        return {
            "_variant": "CompoundPattern",
            "value": value,
            __as_preserve__() {return fromPattern(this);},
            __preserve_on__(e) { e.push(fromPattern(this)); },
            __preserve_text_on__(w) { w.push(fromPattern(this)); }
        };
    };
    CompoundPattern.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Pattern"),
            variant: _.Symbol.for("CompoundPattern")
        };
    };
}

export namespace SimplePattern {
    export function any<_embedded extends _.Embeddable = _.GenericEmbedded>(): SimplePattern<_embedded> {
        return {
            "_variant": "any",
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    any.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("any")
        };
    };
    export function atom<_embedded extends _.Embeddable = _.GenericEmbedded>(atomKind: AtomKind): SimplePattern<_embedded> {
        return {
            "_variant": "atom",
            "atomKind": atomKind,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    atom.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("atom")
        };
    };
    export function embedded<_embedded extends _.Embeddable = _.GenericEmbedded>($interface: SimplePattern<_embedded>): SimplePattern<_embedded> {
        return {
            "_variant": "embedded",
            "interface": $interface,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    embedded.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("embedded")
        };
    };
    export function lit<_embedded extends _.Embeddable = _.GenericEmbedded>(value: _.Value<_embedded>): SimplePattern<_embedded> {
        return {
            "_variant": "lit",
            "value": value,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    lit.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("lit")
        };
    };
    export function seqof<_embedded extends _.Embeddable = _.GenericEmbedded>(pattern: SimplePattern<_embedded>): SimplePattern<_embedded> {
        return {
            "_variant": "seqof",
            "pattern": pattern,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    seqof.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("seqof")
        };
    };
    export function setof<_embedded extends _.Embeddable = _.GenericEmbedded>(pattern: SimplePattern<_embedded>): SimplePattern<_embedded> {
        return {
            "_variant": "setof",
            "pattern": pattern,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    setof.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("setof")
        };
    };
    export function dictof<_embedded extends _.Embeddable = _.GenericEmbedded>({key, value}: {key: SimplePattern<_embedded>, value: SimplePattern<_embedded>}): SimplePattern<_embedded> {
        return {
            "_variant": "dictof",
            "key": key,
            "value": value,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    dictof.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("dictof")
        };
    };
    export function Ref<_embedded extends _.Embeddable = _.GenericEmbedded>(value: Ref): SimplePattern<_embedded> {
        return {
            "_variant": "Ref",
            "value": value,
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    Ref.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SimplePattern"),
            variant: _.Symbol.for("Ref")
        };
    };
}

export namespace CompoundPattern {
    export function rec<_embedded extends _.Embeddable = _.GenericEmbedded>(
        {label, fields}: {label: NamedPattern<_embedded>, fields: NamedPattern<_embedded>}
    ): CompoundPattern<_embedded> {
        return {
            "_variant": "rec",
            "label": label,
            "fields": fields,
            __as_preserve__() {return fromCompoundPattern(this);},
            __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
            __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
        };
    };
    rec.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("CompoundPattern"),
            variant: _.Symbol.for("rec")
        };
    };
    export function tuple<_embedded extends _.Embeddable = _.GenericEmbedded>(patterns: Array<NamedPattern<_embedded>>): CompoundPattern<_embedded> {
        return {
            "_variant": "tuple",
            "patterns": patterns,
            __as_preserve__() {return fromCompoundPattern(this);},
            __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
            __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
        };
    };
    tuple.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("CompoundPattern"),
            variant: _.Symbol.for("tuple")
        };
    };
    export function tuplePrefix<_embedded extends _.Embeddable = _.GenericEmbedded>(
        {fixed, variable}: {fixed: Array<NamedPattern<_embedded>>, variable: NamedSimplePattern<_embedded>}
    ): CompoundPattern<_embedded> {
        return {
            "_variant": "tuplePrefix",
            "fixed": fixed,
            "variable": variable,
            __as_preserve__() {return fromCompoundPattern(this);},
            __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
            __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
        };
    };
    tuplePrefix.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("CompoundPattern"),
            variant: _.Symbol.for("tuplePrefix")
        };
    };
    export function dict<_embedded extends _.Embeddable = _.GenericEmbedded>(entries: DictionaryEntries<_embedded>): CompoundPattern<_embedded> {
        return {
            "_variant": "dict",
            "entries": entries,
            __as_preserve__() {return fromCompoundPattern(this);},
            __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
            __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
        };
    };
    dict.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("CompoundPattern"),
            variant: _.Symbol.for("dict")
        };
    };
}

export function DictionaryEntries<_embedded extends _.Embeddable = _.GenericEmbedded>(
    value: _.EncodableDictionary<_embedded, _.Value<_embedded>, NamedSimplePattern<_embedded>>
): DictionaryEntries<_embedded> {return value;}

DictionaryEntries.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("DictionaryEntries")
    };
}

export namespace AtomKind {
    export function Boolean(): AtomKind {
        return {
            "_variant": "Boolean",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    Boolean.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("AtomKind"),
            variant: _.Symbol.for("Boolean")
        };
    };
    export function Double(): AtomKind {
        return {
            "_variant": "Double",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    Double.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("AtomKind"),
            variant: _.Symbol.for("Double")
        };
    };
    export function SignedInteger(): AtomKind {
        return {
            "_variant": "SignedInteger",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    SignedInteger.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("AtomKind"),
            variant: _.Symbol.for("SignedInteger")
        };
    };
    export function String(): AtomKind {
        return {
            "_variant": "String",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    String.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("AtomKind"),
            variant: _.Symbol.for("String")
        };
    };
    export function ByteString(): AtomKind {
        return {
            "_variant": "ByteString",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    ByteString.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("AtomKind"),
            variant: _.Symbol.for("ByteString")
        };
    };
    export function Symbol(): AtomKind {
        return {
            "_variant": "Symbol",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    Symbol.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("AtomKind"),
            variant: _.Symbol.for("Symbol")
        };
    };
}

export function NamedAlternative<_embedded extends _.Embeddable = _.GenericEmbedded>({variantLabel, pattern}: {variantLabel: string, pattern: Pattern<_embedded>}): NamedAlternative<_embedded> {
    return {
        "variantLabel": variantLabel,
        "pattern": pattern,
        __as_preserve__() {return fromNamedAlternative(this);},
        __preserve_on__(e) { e.push(fromNamedAlternative(this)); },
        __preserve_text_on__(w) { w.push(fromNamedAlternative(this)); }
    };
}

NamedAlternative.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("NamedAlternative")
    };
}

export namespace NamedSimplePattern {
    export function named<_embedded extends _.Embeddable = _.GenericEmbedded>(value: Binding<_embedded>): NamedSimplePattern<_embedded> {
        return {
            "_variant": "named",
            "value": value,
            __as_preserve__() {return fromNamedSimplePattern(this);},
            __preserve_on__(e) { e.push(fromNamedSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromNamedSimplePattern(this)); }
        };
    };
    named.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("NamedSimplePattern"),
            variant: _.Symbol.for("named")
        };
    };
    export function anonymous<_embedded extends _.Embeddable = _.GenericEmbedded>(value: SimplePattern<_embedded>): NamedSimplePattern<_embedded> {
        return {
            "_variant": "anonymous",
            "value": value,
            __as_preserve__() {return fromNamedSimplePattern(this);},
            __preserve_on__(e) { e.push(fromNamedSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromNamedSimplePattern(this)); }
        };
    };
    anonymous.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("NamedSimplePattern"),
            variant: _.Symbol.for("anonymous")
        };
    };
}

export namespace NamedPattern {
    export function named<_embedded extends _.Embeddable = _.GenericEmbedded>(value: Binding<_embedded>): NamedPattern<_embedded> {
        return {
            "_variant": "named",
            "value": value,
            __as_preserve__() {return fromNamedPattern(this);},
            __preserve_on__(e) { e.push(fromNamedPattern(this)); },
            __preserve_text_on__(w) { w.push(fromNamedPattern(this)); }
        };
    };
    named.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("NamedPattern"),
            variant: _.Symbol.for("named")
        };
    };
    export function anonymous<_embedded extends _.Embeddable = _.GenericEmbedded>(value: Pattern<_embedded>): NamedPattern<_embedded> {
        return {
            "_variant": "anonymous",
            "value": value,
            __as_preserve__() {return fromNamedPattern(this);},
            __preserve_on__(e) { e.push(fromNamedPattern(this)); },
            __preserve_text_on__(w) { w.push(fromNamedPattern(this)); }
        };
    };
    anonymous.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("NamedPattern"),
            variant: _.Symbol.for("anonymous")
        };
    };
}

export function Binding<_embedded extends _.Embeddable = _.GenericEmbedded>({name, pattern}: {name: symbol, pattern: SimplePattern<_embedded>}): Binding<_embedded> {
    return {
        "name": name,
        "pattern": pattern,
        __as_preserve__() {return fromBinding(this);},
        __preserve_on__(e) { e.push(fromBinding(this)); },
        __preserve_text_on__(w) { w.push(fromBinding(this)); }
    };
}

Binding.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Binding")};
}

export function Ref({module, name}: {module: ModulePath, name: symbol}): Ref {
    return {
        "module": module,
        "name": name,
        __as_preserve__() {return fromRef(this);},
        __preserve_on__(e) { e.push(fromRef(this)); },
        __preserve_text_on__(w) { w.push(fromRef(this)); }
    };
}

Ref.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Ref")};
}

export function ModulePath(value: Array<symbol>): ModulePath {return value;}

ModulePath.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("ModulePath")
    };
}

export function asBundle<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Bundle<_embedded> {
    let result = toBundle(v);
    if (result === void 0) throw new TypeError(`Invalid Bundle: ${_.stringify(v)}`);
    return result;
}

export function toBundle<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Bundle<_embedded> {
    let result: undefined | Bundle<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $bundle) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Modules<_embedded>) | undefined;
            _tmp1 = toModules(v[0]);
            if (_tmp1 !== void 0) {
                result = {
                    "modules": _tmp1,
                    __as_preserve__() {return fromBundle(this);},
                    __preserve_on__(e) { e.push(fromBundle(this)); },
                    __preserve_text_on__(w) { w.push(fromBundle(this)); }
                };
            };
        };
    };
    return result;
}

Bundle.__from_preserve__ = toBundle;

export function fromBundle<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Bundle<_embedded>): _.Value<_embedded> {return _.Record($bundle, [fromModules<_embedded>(_v["modules"])]);}

export function asModules<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Modules<_embedded> {
    let result = toModules(v);
    if (result === void 0) throw new TypeError(`Invalid Modules: ${_.stringify(v)}`);
    return result;
}

export function toModules<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Modules<_embedded> {
    let _tmp0: (_.EncodableDictionary<_embedded, ModulePath, Schema<_embedded>>) | undefined;
    let result: undefined | Modules<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        const _tmp1 = new _.DictionaryMap(v);
        _tmp0 = new _.EncodableDictionary<_embedded, ModulePath, Schema<_embedded>>(fromModulePath, fromSchema<_embedded>);
        for (const [_tmp2, _tmp3] of _tmp1) {
            let _tmp4: (ModulePath) | undefined;
            _tmp4 = toModulePath(_tmp2);
            if (_tmp4 !== void 0) {
                let _tmp5: (Schema<_embedded>) | undefined;
                _tmp5 = toSchema(_tmp3);
                if (_tmp5 !== void 0) {_tmp0.set(_tmp4, _tmp5); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

Modules.__from_preserve__ = toModules;

export function fromModules<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Modules<_embedded>): _.Value<_embedded> {
    return _.Dictionary.from<_embedded>(
        _.Array.from(_v.entries()).map(([k, v]) => [fromModulePath<_embedded>(k), fromSchema<_embedded>(v)])
    );
}

export function asSchema<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Schema<_embedded> {
    let result = toSchema(v);
    if (result === void 0) throw new TypeError(`Invalid Schema: ${_.stringify(v)}`);
    return result;
}

export function toSchema<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Schema<_embedded> {
    let result: undefined | Schema<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $schema) ? {} : void 0;
        if (_tmp0 !== void 0) {
            if (_.Dictionary.isDictionary<_embedded>(v[0])) {
                let _tmp2: (_.Value<_embedded>) | undefined;
                const _tmp1 = new _.DictionaryMap(v[0]);
                if ((_tmp2 = _tmp1.get($version)) !== void 0) {
                    let _tmp3: (Version) | undefined;
                    _tmp3 = toVersion(_tmp2);
                    if (_tmp3 !== void 0) {
                        let _tmp4: (_.Value<_embedded>) | undefined;
                        if ((_tmp4 = _tmp1.get($embeddedType)) !== void 0) {
                            let _tmp5: (EmbeddedTypeName) | undefined;
                            _tmp5 = toEmbeddedTypeName(_tmp4);
                            if (_tmp5 !== void 0) {
                                let _tmp6: (_.Value<_embedded>) | undefined;
                                if ((_tmp6 = _tmp1.get($definitions)) !== void 0) {
                                    let _tmp7: (Definitions<_embedded>) | undefined;
                                    _tmp7 = toDefinitions(_tmp6);
                                    if (_tmp7 !== void 0) {
                                        result = {
                                            "version": _tmp3,
                                            "embeddedType": _tmp5,
                                            "definitions": _tmp7,
                                            __as_preserve__() {return fromSchema(this);},
                                            __preserve_on__(e) { e.push(fromSchema(this)); },
                                            __preserve_text_on__(w) { w.push(fromSchema(this)); }
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

Schema.__from_preserve__ = toSchema;

export function fromSchema<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Schema<_embedded>): _.Value<_embedded> {
    return _.Record(
        $schema,
        [
            _.Dictionary.from<_embedded>(
                [
                    [$version, fromVersion<_embedded>(_v["version"])],
                    [$embeddedType, fromEmbeddedTypeName<_embedded>(_v["embeddedType"])],
                    [$definitions, fromDefinitions<_embedded>(_v["definitions"])]
                ]
            )
        ]
    );
}

export function asVersion<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Version {
    let result = toVersion(v);
    if (result === void 0) throw new TypeError(`Invalid Version: ${_.stringify(v)}`);
    return result;
}

export function toVersion<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Version {
    let _tmp0: ({}) | undefined;
    let result: undefined | Version;
    _tmp0 = _.is(v, $1) ? {} : void 0;
    if (_tmp0 !== void 0) {
        result = {
            __as_preserve__() {return fromVersion(this);},
            __preserve_on__(e) { e.push(fromVersion(this)); },
            __preserve_text_on__(w) { w.push(fromVersion(this)); }
        };
    };
    return result;
}

Version.__from_preserve__ = toVersion;

export function fromVersion<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Version): _.Value<_embedded> {return $1;}

export function asEmbeddedTypeName<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): EmbeddedTypeName {
    let result = toEmbeddedTypeName(v);
    if (result === void 0) throw new TypeError(`Invalid EmbeddedTypeName: ${_.stringify(v)}`);
    return result;
}

export function toEmbeddedTypeName<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | EmbeddedTypeName {
    let _tmp0: ({}) | undefined;
    let result: undefined | EmbeddedTypeName;
    _tmp0 = _.is(v, __lit6) ? {} : void 0;
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "false",
            __as_preserve__() {return fromEmbeddedTypeName(this);},
            __preserve_on__(e) { e.push(fromEmbeddedTypeName(this)); },
            __preserve_text_on__(w) { w.push(fromEmbeddedTypeName(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: (Ref) | undefined;
        _tmp1 = toRef(v);
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "Ref",
                "value": _tmp1,
                __as_preserve__() {return fromEmbeddedTypeName(this);},
                __preserve_on__(e) { e.push(fromEmbeddedTypeName(this)); },
                __preserve_text_on__(w) { w.push(fromEmbeddedTypeName(this)); }
            };
        };
    };
    return result;
}

export namespace EmbeddedTypeName {export const __from_preserve__ = toEmbeddedTypeName;}

export function fromEmbeddedTypeName<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: EmbeddedTypeName): _.Value<_embedded> {
    switch (_v._variant) {
        case "false": {return __lit6;};
        case "Ref": {return fromRef<_embedded>(_v.value);};
    };
}

export function asDefinitions<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Definitions<_embedded> {
    let result = toDefinitions(v);
    if (result === void 0) throw new TypeError(`Invalid Definitions: ${_.stringify(v)}`);
    return result;
}

export function toDefinitions<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Definitions<_embedded> {
    let _tmp0: (_.JsDictionary<Definition<_embedded>>) | undefined;
    let result: undefined | Definitions<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        const _tmp1 = new _.DictionaryMap(v);
        _tmp0 = {};
        for (const [_tmp2, _tmp3] of _tmp1) {
            let _tmp4: (symbol) | undefined;
            _tmp4 = typeof _tmp2 === 'symbol' ? _tmp2 : void 0;
            if (_tmp4 !== void 0) {
                let _tmp5: (Definition<_embedded>) | undefined;
                _tmp5 = toDefinition(_tmp3);
                if (_tmp5 !== void 0) {_tmp0[_tmp4.description!] = _tmp5; continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

Definitions.__from_preserve__ = toDefinitions;

export function fromDefinitions<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Definitions<_embedded>): _.Value<_embedded> {
    return _.Dictionary.from<_embedded>(
        _.Array.from(_.JsDictionary.entries(_v)).map(([k, v]) => [k, fromDefinition<_embedded>(v)])
    );
}

export function asDefinition<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Definition<_embedded> {
    let result = toDefinition(v);
    if (result === void 0) throw new TypeError(`Invalid Definition: ${_.stringify(v)}`);
    return result;
}

export function toDefinition<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Definition<_embedded> {
    let result: undefined | Definition<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $or) ? {} : void 0;
        if (_tmp0 !== void 0) {
            if (_.isSequence(v[0]) && v[0].length >= 2) {
                let _tmp1: (NamedAlternative<_embedded>) | undefined;
                _tmp1 = toNamedAlternative(v[0][0]);
                if (_tmp1 !== void 0) {
                    let _tmp2: (NamedAlternative<_embedded>) | undefined;
                    _tmp2 = toNamedAlternative(v[0][1]);
                    if (_tmp2 !== void 0) {
                        let _tmp3: (Array<_.Value<_embedded>>) | undefined;
                        let _tmp4: (Array<NamedAlternative<_embedded>>) | undefined;
                        _tmp3 = v[0].slice(2);
                        _tmp4 = [];
                        for (const _tmp5 of _tmp3) {
                            let _tmp6: (NamedAlternative<_embedded>) | undefined;
                            _tmp6 = toNamedAlternative(_tmp5);
                            if (_tmp6 !== void 0) {_tmp4.push(_tmp6); continue;};
                            _tmp4 = void 0;
                            break;
                        };
                        if (_tmp4 !== void 0) {
                            result = {
                                "_variant": "or",
                                "pattern0": _tmp1,
                                "pattern1": _tmp2,
                                "patternN": _tmp4,
                                __as_preserve__() {return fromDefinition(this);},
                                __preserve_on__(e) { e.push(fromDefinition(this)); },
                                __preserve_text_on__(w) { w.push(fromDefinition(this)); }
                            };
                        };
                    };
                };
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp7: ({}) | undefined;
            _tmp7 = _.is(v.label, $and) ? {} : void 0;
            if (_tmp7 !== void 0) {
                if (_.isSequence(v[0]) && v[0].length >= 2) {
                    let _tmp8: (NamedPattern<_embedded>) | undefined;
                    _tmp8 = toNamedPattern(v[0][0]);
                    if (_tmp8 !== void 0) {
                        let _tmp9: (NamedPattern<_embedded>) | undefined;
                        _tmp9 = toNamedPattern(v[0][1]);
                        if (_tmp9 !== void 0) {
                            let _tmp10: (Array<_.Value<_embedded>>) | undefined;
                            let _tmp11: (Array<NamedPattern<_embedded>>) | undefined;
                            _tmp10 = v[0].slice(2);
                            _tmp11 = [];
                            for (const _tmp12 of _tmp10) {
                                let _tmp13: (NamedPattern<_embedded>) | undefined;
                                _tmp13 = toNamedPattern(_tmp12);
                                if (_tmp13 !== void 0) {_tmp11.push(_tmp13); continue;};
                                _tmp11 = void 0;
                                break;
                            };
                            if (_tmp11 !== void 0) {
                                result = {
                                    "_variant": "and",
                                    "pattern0": _tmp8,
                                    "pattern1": _tmp9,
                                    "patternN": _tmp11,
                                    __as_preserve__() {return fromDefinition(this);},
                                    __preserve_on__(e) { e.push(fromDefinition(this)); },
                                    __preserve_text_on__(w) { w.push(fromDefinition(this)); }
                                };
                            };
                        };
                    };
                };
            };
        };
        if (result === void 0) {
            let _tmp14: (Pattern<_embedded>) | undefined;
            _tmp14 = toPattern(v);
            if (_tmp14 !== void 0) {
                result = {
                    "_variant": "Pattern",
                    "value": _tmp14,
                    __as_preserve__() {return fromDefinition(this);},
                    __preserve_on__(e) { e.push(fromDefinition(this)); },
                    __preserve_text_on__(w) { w.push(fromDefinition(this)); }
                };
            };
        };
    };
    return result;
}

export namespace Definition {export const __from_preserve__ = toDefinition;}

export function fromDefinition<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Definition<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "or": {
            return _.Record(
                $or,
                [
                    [
                        fromNamedAlternative<_embedded>(_v["pattern0"]),
                        fromNamedAlternative<_embedded>(_v["pattern1"]),
                        ... _v["patternN"].map(v => fromNamedAlternative<_embedded>(v))
                    ]
                ]
            );
        };
        case "and": {
            return _.Record(
                $and,
                [
                    [
                        fromNamedPattern<_embedded>(_v["pattern0"]),
                        fromNamedPattern<_embedded>(_v["pattern1"]),
                        ... _v["patternN"].map(v => fromNamedPattern<_embedded>(v))
                    ]
                ]
            );
        };
        case "Pattern": {return fromPattern<_embedded>(_v.value);};
    };
}

export function asPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Pattern<_embedded> {
    let result = toPattern(v);
    if (result === void 0) throw new TypeError(`Invalid Pattern: ${_.stringify(v)}`);
    return result;
}

export function toPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Pattern<_embedded> {
    let _tmp0: (SimplePattern<_embedded>) | undefined;
    let result: undefined | Pattern<_embedded>;
    _tmp0 = toSimplePattern(v);
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "SimplePattern",
            "value": _tmp0,
            __as_preserve__() {return fromPattern(this);},
            __preserve_on__(e) { e.push(fromPattern(this)); },
            __preserve_text_on__(w) { w.push(fromPattern(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: (CompoundPattern<_embedded>) | undefined;
        _tmp1 = toCompoundPattern(v);
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "CompoundPattern",
                "value": _tmp1,
                __as_preserve__() {return fromPattern(this);},
                __preserve_on__(e) { e.push(fromPattern(this)); },
                __preserve_text_on__(w) { w.push(fromPattern(this)); }
            };
        };
    };
    return result;
}

export namespace Pattern {export const __from_preserve__ = toPattern;}

export function fromPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Pattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "SimplePattern": {return fromSimplePattern<_embedded>(_v.value);};
        case "CompoundPattern": {return fromCompoundPattern<_embedded>(_v.value);};
    };
}

export function asSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): SimplePattern<_embedded> {
    let result = toSimplePattern(v);
    if (result === void 0) throw new TypeError(`Invalid SimplePattern: ${_.stringify(v)}`);
    return result;
}

export function toSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SimplePattern<_embedded> {
    let _tmp0: ({}) | undefined;
    let result: undefined | SimplePattern<_embedded>;
    _tmp0 = _.is(v, $any) ? {} : void 0;
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "any",
            __as_preserve__() {return fromSimplePattern(this);},
            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp1: ({}) | undefined;
            _tmp1 = _.is(v.label, $atom) ? {} : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (AtomKind) | undefined;
                _tmp2 = toAtomKind(v[0]);
                if (_tmp2 !== void 0) {
                    result = {
                        "_variant": "atom",
                        "atomKind": _tmp2,
                        __as_preserve__() {return fromSimplePattern(this);},
                        __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                        __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                    };
                };
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp3: ({}) | undefined;
                _tmp3 = _.is(v.label, $embedded) ? {} : void 0;
                if (_tmp3 !== void 0) {
                    let _tmp4: (SimplePattern<_embedded>) | undefined;
                    _tmp4 = toSimplePattern(v[0]);
                    if (_tmp4 !== void 0) {
                        result = {
                            "_variant": "embedded",
                            "interface": _tmp4,
                            __as_preserve__() {return fromSimplePattern(this);},
                            __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                            __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                        };
                    };
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp5: ({}) | undefined;
                    _tmp5 = _.is(v.label, $lit) ? {} : void 0;
                    if (_tmp5 !== void 0) {
                        let _tmp6: (_.Value<_embedded>) | undefined;
                        _tmp6 = v[0];
                        if (_tmp6 !== void 0) {
                            result = {
                                "_variant": "lit",
                                "value": _tmp6,
                                __as_preserve__() {return fromSimplePattern(this);},
                                __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                                __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                            };
                        };
                    };
                };
                if (result === void 0) {
                    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                        let _tmp7: ({}) | undefined;
                        _tmp7 = _.is(v.label, $seqof) ? {} : void 0;
                        if (_tmp7 !== void 0) {
                            let _tmp8: (SimplePattern<_embedded>) | undefined;
                            _tmp8 = toSimplePattern(v[0]);
                            if (_tmp8 !== void 0) {
                                result = {
                                    "_variant": "seqof",
                                    "pattern": _tmp8,
                                    __as_preserve__() {return fromSimplePattern(this);},
                                    __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                                    __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                                };
                            };
                        };
                    };
                    if (result === void 0) {
                        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                            let _tmp9: ({}) | undefined;
                            _tmp9 = _.is(v.label, $setof) ? {} : void 0;
                            if (_tmp9 !== void 0) {
                                let _tmp10: (SimplePattern<_embedded>) | undefined;
                                _tmp10 = toSimplePattern(v[0]);
                                if (_tmp10 !== void 0) {
                                    result = {
                                        "_variant": "setof",
                                        "pattern": _tmp10,
                                        __as_preserve__() {return fromSimplePattern(this);},
                                        __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                                        __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                                    };
                                };
                            };
                        };
                        if (result === void 0) {
                            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                                let _tmp11: ({}) | undefined;
                                _tmp11 = _.is(v.label, $dictof) ? {} : void 0;
                                if (_tmp11 !== void 0) {
                                    let _tmp12: (SimplePattern<_embedded>) | undefined;
                                    _tmp12 = toSimplePattern(v[0]);
                                    if (_tmp12 !== void 0) {
                                        let _tmp13: (SimplePattern<_embedded>) | undefined;
                                        _tmp13 = toSimplePattern(v[1]);
                                        if (_tmp13 !== void 0) {
                                            result = {
                                                "_variant": "dictof",
                                                "key": _tmp12,
                                                "value": _tmp13,
                                                __as_preserve__() {return fromSimplePattern(this);},
                                                __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                                                __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                                            };
                                        };
                                    };
                                };
                            };
                            if (result === void 0) {
                                let _tmp14: (Ref) | undefined;
                                _tmp14 = toRef(v);
                                if (_tmp14 !== void 0) {
                                    result = {
                                        "_variant": "Ref",
                                        "value": _tmp14,
                                        __as_preserve__() {return fromSimplePattern(this);},
                                        __preserve_on__(e) { e.push(fromSimplePattern(this)); },
                                        __preserve_text_on__(w) { w.push(fromSimplePattern(this)); }
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export namespace SimplePattern {export const __from_preserve__ = toSimplePattern;}

export function fromSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: SimplePattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "any": {return $any;};
        case "atom": {return _.Record($atom, [fromAtomKind<_embedded>(_v["atomKind"])]);};
        case "embedded": {return _.Record($embedded, [fromSimplePattern<_embedded>(_v["interface"])]);};
        case "lit": {return _.Record($lit, [_v["value"]]);};
        case "seqof": {return _.Record($seqof, [fromSimplePattern<_embedded>(_v["pattern"])]);};
        case "setof": {return _.Record($setof, [fromSimplePattern<_embedded>(_v["pattern"])]);};
        case "dictof": {
            return _.Record(
                $dictof,
                [
                    fromSimplePattern<_embedded>(_v["key"]),
                    fromSimplePattern<_embedded>(_v["value"])
                ]
            );
        };
        case "Ref": {return fromRef<_embedded>(_v.value);};
    };
}

export function asCompoundPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): CompoundPattern<_embedded> {
    let result = toCompoundPattern(v);
    if (result === void 0) throw new TypeError(`Invalid CompoundPattern: ${_.stringify(v)}`);
    return result;
}

export function toCompoundPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | CompoundPattern<_embedded> {
    let result: undefined | CompoundPattern<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $rec) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (NamedPattern<_embedded>) | undefined;
            _tmp1 = toNamedPattern(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (NamedPattern<_embedded>) | undefined;
                _tmp2 = toNamedPattern(v[1]);
                if (_tmp2 !== void 0) {
                    result = {
                        "_variant": "rec",
                        "label": _tmp1,
                        "fields": _tmp2,
                        __as_preserve__() {return fromCompoundPattern(this);},
                        __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
                        __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
                    };
                };
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp3: ({}) | undefined;
            _tmp3 = _.is(v.label, $tuple) ? {} : void 0;
            if (_tmp3 !== void 0) {
                let _tmp4: (Array<NamedPattern<_embedded>>) | undefined;
                _tmp4 = void 0;
                if (_.isSequence(v[0])) {
                    _tmp4 = [];
                    for (const _tmp5 of v[0]) {
                        let _tmp6: (NamedPattern<_embedded>) | undefined;
                        _tmp6 = toNamedPattern(_tmp5);
                        if (_tmp6 !== void 0) {_tmp4.push(_tmp6); continue;};
                        _tmp4 = void 0;
                        break;
                    };
                };
                if (_tmp4 !== void 0) {
                    result = {
                        "_variant": "tuple",
                        "patterns": _tmp4,
                        __as_preserve__() {return fromCompoundPattern(this);},
                        __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
                        __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
                    };
                };
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp7: ({}) | undefined;
                _tmp7 = _.is(v.label, $tuplePrefix) ? {} : void 0;
                if (_tmp7 !== void 0) {
                    let _tmp8: (Array<NamedPattern<_embedded>>) | undefined;
                    _tmp8 = void 0;
                    if (_.isSequence(v[0])) {
                        _tmp8 = [];
                        for (const _tmp9 of v[0]) {
                            let _tmp10: (NamedPattern<_embedded>) | undefined;
                            _tmp10 = toNamedPattern(_tmp9);
                            if (_tmp10 !== void 0) {_tmp8.push(_tmp10); continue;};
                            _tmp8 = void 0;
                            break;
                        };
                    };
                    if (_tmp8 !== void 0) {
                        let _tmp11: (NamedSimplePattern<_embedded>) | undefined;
                        _tmp11 = toNamedSimplePattern(v[1]);
                        if (_tmp11 !== void 0) {
                            result = {
                                "_variant": "tuplePrefix",
                                "fixed": _tmp8,
                                "variable": _tmp11,
                                __as_preserve__() {return fromCompoundPattern(this);},
                                __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
                                __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
                            };
                        };
                    };
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp12: ({}) | undefined;
                    _tmp12 = _.is(v.label, $dict) ? {} : void 0;
                    if (_tmp12 !== void 0) {
                        let _tmp13: (DictionaryEntries<_embedded>) | undefined;
                        _tmp13 = toDictionaryEntries(v[0]);
                        if (_tmp13 !== void 0) {
                            result = {
                                "_variant": "dict",
                                "entries": _tmp13,
                                __as_preserve__() {return fromCompoundPattern(this);},
                                __preserve_on__(e) { e.push(fromCompoundPattern(this)); },
                                __preserve_text_on__(w) { w.push(fromCompoundPattern(this)); }
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export namespace CompoundPattern {export const __from_preserve__ = toCompoundPattern;}

export function fromCompoundPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: CompoundPattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "rec": {
            return _.Record(
                $rec,
                [
                    fromNamedPattern<_embedded>(_v["label"]),
                    fromNamedPattern<_embedded>(_v["fields"])
                ]
            );
        };
        case "tuple": {
            return _.Record($tuple, [_v["patterns"].map(v => fromNamedPattern<_embedded>(v))]);
        };
        case "tuplePrefix": {
            return _.Record(
                $tuplePrefix,
                [
                    _v["fixed"].map(v => fromNamedPattern<_embedded>(v)),
                    fromNamedSimplePattern<_embedded>(_v["variable"])
                ]
            );
        };
        case "dict": {return _.Record($dict, [fromDictionaryEntries<_embedded>(_v["entries"])]);};
    };
}

export function asDictionaryEntries<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): DictionaryEntries<_embedded> {
    let result = toDictionaryEntries(v);
    if (result === void 0) throw new TypeError(`Invalid DictionaryEntries: ${_.stringify(v)}`);
    return result;
}

export function toDictionaryEntries<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | DictionaryEntries<_embedded> {
    let _tmp0: (_.EncodableDictionary<_embedded, _.Value<_embedded>, NamedSimplePattern<_embedded>>) | undefined;
    let result: undefined | DictionaryEntries<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        const _tmp1 = new _.DictionaryMap(v);
        _tmp0 = new _.EncodableDictionary<_embedded, _.Value<_embedded>, NamedSimplePattern<_embedded>>(k => k, fromNamedSimplePattern<_embedded>);
        for (const [_tmp2, _tmp3] of _tmp1) {
            let _tmp4: (_.Value<_embedded>) | undefined;
            _tmp4 = _tmp2;
            if (_tmp4 !== void 0) {
                let _tmp5: (NamedSimplePattern<_embedded>) | undefined;
                _tmp5 = toNamedSimplePattern(_tmp3);
                if (_tmp5 !== void 0) {_tmp0.set(_tmp4, _tmp5); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

DictionaryEntries.__from_preserve__ = toDictionaryEntries;

export function fromDictionaryEntries<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: DictionaryEntries<_embedded>): _.Value<_embedded> {
    return _.Dictionary.from<_embedded>(
        _.Array.from(_v.entries()).map(([k, v]) => [k, fromNamedSimplePattern<_embedded>(v)])
    );
}

export function asAtomKind<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): AtomKind {
    let result = toAtomKind(v);
    if (result === void 0) throw new TypeError(`Invalid AtomKind: ${_.stringify(v)}`);
    return result;
}

export function toAtomKind<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | AtomKind {
    let _tmp0: ({}) | undefined;
    let result: undefined | AtomKind;
    _tmp0 = _.is(v, $Boolean) ? {} : void 0;
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "Boolean",
            __as_preserve__() {return fromAtomKind(this);},
            __preserve_on__(e) { e.push(fromAtomKind(this)); },
            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: ({}) | undefined;
        _tmp1 = _.is(v, $Double) ? {} : void 0;
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "Double",
                __as_preserve__() {return fromAtomKind(this);},
                __preserve_on__(e) { e.push(fromAtomKind(this)); },
                __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
            };
        };
        if (result === void 0) {
            let _tmp2: ({}) | undefined;
            _tmp2 = _.is(v, $SignedInteger) ? {} : void 0;
            if (_tmp2 !== void 0) {
                result = {
                    "_variant": "SignedInteger",
                    __as_preserve__() {return fromAtomKind(this);},
                    __preserve_on__(e) { e.push(fromAtomKind(this)); },
                    __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
                };
            };
            if (result === void 0) {
                let _tmp3: ({}) | undefined;
                _tmp3 = _.is(v, $String) ? {} : void 0;
                if (_tmp3 !== void 0) {
                    result = {
                        "_variant": "String",
                        __as_preserve__() {return fromAtomKind(this);},
                        __preserve_on__(e) { e.push(fromAtomKind(this)); },
                        __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
                    };
                };
                if (result === void 0) {
                    let _tmp4: ({}) | undefined;
                    _tmp4 = _.is(v, $ByteString) ? {} : void 0;
                    if (_tmp4 !== void 0) {
                        result = {
                            "_variant": "ByteString",
                            __as_preserve__() {return fromAtomKind(this);},
                            __preserve_on__(e) { e.push(fromAtomKind(this)); },
                            __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
                        };
                    };
                    if (result === void 0) {
                        let _tmp5: ({}) | undefined;
                        _tmp5 = _.is(v, $Symbol) ? {} : void 0;
                        if (_tmp5 !== void 0) {
                            result = {
                                "_variant": "Symbol",
                                __as_preserve__() {return fromAtomKind(this);},
                                __preserve_on__(e) { e.push(fromAtomKind(this)); },
                                __preserve_text_on__(w) { w.push(fromAtomKind(this)); }
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export namespace AtomKind {export const __from_preserve__ = toAtomKind;}

export function fromAtomKind<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: AtomKind): _.Value<_embedded> {
    switch (_v._variant) {
        case "Boolean": {return $Boolean;};
        case "Double": {return $Double;};
        case "SignedInteger": {return $SignedInteger;};
        case "String": {return $String;};
        case "ByteString": {return $ByteString;};
        case "Symbol": {return $Symbol;};
    };
}

export function asNamedAlternative<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): NamedAlternative<_embedded> {
    let result = toNamedAlternative(v);
    if (result === void 0) throw new TypeError(`Invalid NamedAlternative: ${_.stringify(v)}`);
    return result;
}

export function toNamedAlternative<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedAlternative<_embedded> {
    let result: undefined | NamedAlternative<_embedded>;
    if (_.isSequence(v) && v.length >= 2) {
        let _tmp0: (string) | undefined;
        _tmp0 = typeof v[0] === 'string' ? v[0] : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern<_embedded>) | undefined;
            _tmp1 = toPattern(v[1]);
            if (_tmp1 !== void 0) {
                result = {
                    "variantLabel": _tmp0,
                    "pattern": _tmp1,
                    __as_preserve__() {return fromNamedAlternative(this);},
                    __preserve_on__(e) { e.push(fromNamedAlternative(this)); },
                    __preserve_text_on__(w) { w.push(fromNamedAlternative(this)); }
                };
            };
        };
    };
    return result;
}

NamedAlternative.__from_preserve__ = toNamedAlternative;

export function fromNamedAlternative<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: NamedAlternative<_embedded>): _.Value<_embedded> {return [_v["variantLabel"], fromPattern<_embedded>(_v["pattern"])];}

export function asNamedSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): NamedSimplePattern<_embedded> {
    let result = toNamedSimplePattern(v);
    if (result === void 0) throw new TypeError(`Invalid NamedSimplePattern: ${_.stringify(v)}`);
    return result;
}

export function toNamedSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedSimplePattern<_embedded> {
    let _tmp0: (Binding<_embedded>) | undefined;
    let result: undefined | NamedSimplePattern<_embedded>;
    _tmp0 = toBinding(v);
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "named",
            "value": _tmp0,
            __as_preserve__() {return fromNamedSimplePattern(this);},
            __preserve_on__(e) { e.push(fromNamedSimplePattern(this)); },
            __preserve_text_on__(w) { w.push(fromNamedSimplePattern(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: (SimplePattern<_embedded>) | undefined;
        _tmp1 = toSimplePattern(v);
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "anonymous",
                "value": _tmp1,
                __as_preserve__() {return fromNamedSimplePattern(this);},
                __preserve_on__(e) { e.push(fromNamedSimplePattern(this)); },
                __preserve_text_on__(w) { w.push(fromNamedSimplePattern(this)); }
            };
        };
    };
    return result;
}

export namespace NamedSimplePattern {export const __from_preserve__ = toNamedSimplePattern;}

export function fromNamedSimplePattern<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: NamedSimplePattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "named": {return fromBinding<_embedded>(_v.value);};
        case "anonymous": {return fromSimplePattern<_embedded>(_v.value);};
    };
}

export function asNamedPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): NamedPattern<_embedded> {
    let result = toNamedPattern(v);
    if (result === void 0) throw new TypeError(`Invalid NamedPattern: ${_.stringify(v)}`);
    return result;
}

export function toNamedPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedPattern<_embedded> {
    let _tmp0: (Binding<_embedded>) | undefined;
    let result: undefined | NamedPattern<_embedded>;
    _tmp0 = toBinding(v);
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "named",
            "value": _tmp0,
            __as_preserve__() {return fromNamedPattern(this);},
            __preserve_on__(e) { e.push(fromNamedPattern(this)); },
            __preserve_text_on__(w) { w.push(fromNamedPattern(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: (Pattern<_embedded>) | undefined;
        _tmp1 = toPattern(v);
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "anonymous",
                "value": _tmp1,
                __as_preserve__() {return fromNamedPattern(this);},
                __preserve_on__(e) { e.push(fromNamedPattern(this)); },
                __preserve_text_on__(w) { w.push(fromNamedPattern(this)); }
            };
        };
    };
    return result;
}

export namespace NamedPattern {export const __from_preserve__ = toNamedPattern;}

export function fromNamedPattern<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: NamedPattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "named": {return fromBinding<_embedded>(_v.value);};
        case "anonymous": {return fromPattern<_embedded>(_v.value);};
    };
}

export function asBinding<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Binding<_embedded> {
    let result = toBinding(v);
    if (result === void 0) throw new TypeError(`Invalid Binding: ${_.stringify(v)}`);
    return result;
}

export function toBinding<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Binding<_embedded> {
    let result: undefined | Binding<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $named) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (symbol) | undefined;
            _tmp1 = typeof v[0] === 'symbol' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (SimplePattern<_embedded>) | undefined;
                _tmp2 = toSimplePattern(v[1]);
                if (_tmp2 !== void 0) {
                    result = {
                        "name": _tmp1,
                        "pattern": _tmp2,
                        __as_preserve__() {return fromBinding(this);},
                        __preserve_on__(e) { e.push(fromBinding(this)); },
                        __preserve_text_on__(w) { w.push(fromBinding(this)); }
                    };
                };
            };
        };
    };
    return result;
}

Binding.__from_preserve__ = toBinding;

export function fromBinding<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Binding<_embedded>): _.Value<_embedded> {
    return _.Record($named, [_v["name"], fromSimplePattern<_embedded>(_v["pattern"])]);
}

export function asRef<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Ref {
    let result = toRef(v);
    if (result === void 0) throw new TypeError(`Invalid Ref: ${_.stringify(v)}`);
    return result;
}

export function toRef<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Ref {
    let result: undefined | Ref;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $ref) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (ModulePath) | undefined;
            _tmp1 = toModulePath(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (symbol) | undefined;
                _tmp2 = typeof v[1] === 'symbol' ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    result = {
                        "module": _tmp1,
                        "name": _tmp2,
                        __as_preserve__() {return fromRef(this);},
                        __preserve_on__(e) { e.push(fromRef(this)); },
                        __preserve_text_on__(w) { w.push(fromRef(this)); }
                    };
                };
            };
        };
    };
    return result;
}

Ref.__from_preserve__ = toRef;

export function fromRef<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Ref): _.Value<_embedded> {return _.Record($ref, [fromModulePath<_embedded>(_v["module"]), _v["name"]]);}

export function asModulePath<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): ModulePath {
    let result = toModulePath(v);
    if (result === void 0) throw new TypeError(`Invalid ModulePath: ${_.stringify(v)}`);
    return result;
}

export function toModulePath<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | ModulePath {
    let _tmp0: (Array<symbol>) | undefined;
    let result: undefined | ModulePath;
    _tmp0 = void 0;
    if (_.isSequence(v)) {
        _tmp0 = [];
        for (const _tmp1 of v) {
            let _tmp2: (symbol) | undefined;
            _tmp2 = typeof _tmp1 === 'symbol' ? _tmp1 : void 0;
            if (_tmp2 !== void 0) {_tmp0.push(_tmp2); continue;};
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

ModulePath.__from_preserve__ = toModulePath;

export function fromModulePath<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: ModulePath): _.Value<_embedded> {return _v.map(v => v);}

