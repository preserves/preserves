import { Position } from '@preserves/core';

export class SchemaSyntaxError extends Error {
    readonly pos: Position | null;

    constructor(message: string, pos: Position | null) {
        super(message);
        this.pos = pos;
    }
}
