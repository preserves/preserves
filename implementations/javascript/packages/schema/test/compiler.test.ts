import {  } from '@preserves/core';
import { compile, Meta as M, readSchema } from '../src/index';
import './test-utils';
import { js as format } from 'js-beautify';

function readSingleSchema(source: string): M.Schema {
    return readSchema(`version 1 . ${source}`);
}

function compileSingleSchema(source: string): string {
    return format(compile([], [Symbol.for('test')], readSingleSchema(source), {}));
}

describe('compiler', () => {
    it('basics', () => {
        expect(compileSingleSchema(`X = int .`)).toContain(`\nexport type X = number;\n`);
    });

    it('symbol-keyed dictionary', () => {
        const s = compileSingleSchema(`D = { symbol:any ...:... } .`);
        expect(s).toMatch(/^export type D.*= _.JsDictionary < _.Value < _embedded >> ;$/m);
    });

    it('string-keyed dictionary', () => {
        const s = compileSingleSchema(`D = { string:any ...:... } .`);
        expect(s).toMatch(/^export type D.*= _.EncodableDictionary < _embedded, string, _.Value < _embedded >> ;$/m);
    });

    it('any-keyed dictionary', () => {
        const s = compileSingleSchema(`D = { any:any ...:... } .`);
        expect(s).toMatch(/^export type D.*= _.EncodableDictionary < _embedded, _.Value < _embedded > , _.Value < _embedded >> ;$/m);
    });
});

describe('dictionary patterns', () => {
    it('with constant values against an identifierlike key', () => {
        const s = compileSingleSchema(`D = { "jsonrpc": "2.0", "id": int }`);
        expect(s).not.toMatch('"jsonrpc": _tmp'); // we shouldn't generate code to store into a jsonrpc field
    });
});
