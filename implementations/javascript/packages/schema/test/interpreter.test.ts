import { GenericEmbedded, stringify, fromJS, parse, EncodableDictionary, KeyedDictionary } from '@preserves/core';
import { SchemaInterpreter, readSchema } from '../src/index';
import './test-utils';

describe('interpreter', () => {
    const I = new SchemaInterpreter<GenericEmbedded>();

    const X_schema = readSchema(`
version 1 .
A = <foo> .
B = <bar @v int> .
C = <zot @v int @w int> .
N = int .

U = <class @a int @b int> / <true @c int @d int> .
V = <public @n int> / <private @n int> .
W = <const> / <let> .

D1 = { symbol:A ...:... } .
D2 = { any:A ...:... } .
D3 = { string:A ...:... } .

J = { "jsonrpc": "2.0", "id": int } .
`);
    const X = Symbol.for('X');
    I.env.set([X], X_schema);

    const E = I.moduleTree();

    it('basically works', () => {
        const a = E.X.A();
        const b = E.X.B(22);
        const c = E.X.C({v: 33, w: 44});
        const n = E.X.N(22);
        expect(stringify(a)).toBe('<foo>');
        expect(stringify(b)).toBe('<bar 22>');
        expect(stringify(c)).toBe('<zot 33 44>');
        expect(stringify(n)).toBe('22');
        expect(parse('<foo>')).is(fromJS(a));
        expect(parse('<bar 22>')).is(fromJS(b));
        expect(parse('<zot 33 44>')).is(fromJS(c));
        expect(parse('22')).is(fromJS(n));
        expect(stringify(E.X.asA(fromJS(a)))).toBe('<foo>');
        expect(stringify(E.X.asB(fromJS(b)))).toBe('<bar 22>');
        expect(stringify(E.X.asC(fromJS(c)))).toBe('<zot 33 44>');
        expect(stringify(E.X.asN(fromJS(n)))).toBe('22');
    });

    it('escapes JS keywords', () => {
        expect(Object.keys(E.X.U)).toEqual(['$class', '$true']);
        expect(Object.keys(E.X.V)).toEqual(['$public', '$private']);
        expect(Object.keys(E.X.W)).toEqual(['$const', '$let']);
    });

    const STANDARD_METHODS = ['__as_preserve__', '__preserve_on__', '__preserve_text_on__'];

    it('accepts correct arguments to n-ary variant ctors', () => {
        expect(stringify(E.X.U.$class({ a: 123, b: 234 }))).toBe('<class 123 234>');
        expect(stringify(E.X.U.$true({ c: 123, d: 234 }))).toBe('<true 123 234>');
        expect(Object.keys(E.X.U.$class({ a: 123, b: 234 }))).toEqual([
            '_variant', 'a', 'b', ... STANDARD_METHODS]);
    });

    it('accepts correct arguments to unary variant ctors', () => {
        expect(stringify(E.X.V.$public(123))).toBe('<public 123>');
        expect(stringify(E.X.V.$private(123))).toBe('<private 123>');
        expect(Object.keys(E.X.V.$public(123))).toEqual([
            '_variant', 'n', ... STANDARD_METHODS]);
    });

    it('accepts correct arguments to nullary variant ctors', () => {
        expect(stringify(E.X.W.$const())).toBe('<const>');
        expect(stringify(E.X.W.$let())).toBe('<let>');
        expect(Object.keys(E.X.W.$const())).toEqual([
            '_variant', ... STANDARD_METHODS]);
    });

    it('produces JsDictionary for symbol-keyed dicts', () => {
        const v = E.X.asD1(parse('{ a: <foo>, b: <foo>}'));
        expect(Object.keys(v)).toEqual(['a', 'b']);
        expect(stringify(E.X.fromA(v.a))).toBe('<foo>');
        expect(stringify(E.X.fromA(v.b))).toBe('<foo>');
    });

    it('produces EncodableDictionary for any-keyed dicts', () => {
        const d2 = E.X.asD2(parse('{ a: <foo>, b: <foo>}'));
        expect(d2 instanceof EncodableDictionary).toBe(true);
        expect(Array.from(d2.keys())).toEqual([Symbol.for('a'), Symbol.for('b')]);
        expect(fromJS(d2.get(Symbol.for('a')))).is(fromJS(E.X.A()));
    });

    it('accepts either kind of dictionary for symbol-keyed dicts', () => {
        const v = { a: E.X.A(), b: E.X.A() };
        expect(stringify(v)).toBe('{a: <foo> b: <foo>}');
        expect(E.X.fromD1(v)).is(parse('{a: <foo> b: <foo>}'));
        expect(E.X.fromD1(E.X.D1(v))).is(parse('{a: <foo> b: <foo>}'));
        expect(E.X.fromD1(E.X.D1(new KeyedDictionary([
            [parse('a'), parse('<foo>')],
            [parse('b'), parse('<foo>')],
        ])))).is(parse('{a: <foo> b: <foo>}'));
    });

    it('accepts either kind of dictionary for any-keyed dicts', () => {
        const v = { a: E.X.A(), b: E.X.A() };
        expect(stringify(v)).toBe('{a: <foo> b: <foo>}');
        expect(E.X.fromD2(v)).is(parse('{a: <foo> b: <foo>}'));
        expect(E.X.fromD2(E.X.D2(v))).is(parse('{a: <foo> b: <foo>}'));
        expect(E.X.fromD2(E.X.D2(new KeyedDictionary([
            [parse('a'), parse('<foo>')],
            [parse('b'), parse('<foo>')],
        ])))).is(parse('{a: <foo> b: <foo>}'));
    });

    it('only includes fields that have useful information', () => {
        const v = E.X.asJ(parse('{"jsonrpc": "2.0", "id": 123}'));
        expect('id' in v).toBe(true);
        expect(v.id).toBe(123);
        expect('jsonrpc' in v).toBe(false);
    });
});
