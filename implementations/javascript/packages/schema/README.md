# Preserves Schema for TypeScript/JavaScript

This is an implementation of [Preserves Schema](https://preserves.dev/preserves-schema.html)
for TypeScript and JavaScript.

This package implements a Schema runtime and a Schema-to-TypeScript compiler, but offers no
command line interfaces. See `@preserves/schema-cli` for command-line tools for working with
Schema and compiling from Schema to TypeScript.
