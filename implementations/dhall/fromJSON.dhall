{-|
Translate a `JSON` value to a `Preserves` value
-}
let Prelude = ./Prelude.dhall

let List/map = Prelude.List.map

let JSON = Prelude.JSON.Type

let Preserves = ./Type.dhall

let Preserves/function = ./function.dhall

let fromJSON
    : JSON → Preserves
    = λ(json : JSON) →
      λ(Preserves : Type) →
      λ(value : Preserves/function Preserves) →
        json
          Preserves
          { array = value.sequence
          , bool = λ(x : Bool) → value.symbol (if x then "true" else "false")
          , double = value.double
          , integer = value.integer
          , null = value.symbol "null"
          , object =
              let Entry = { mapKey : Text, mapValue : Preserves }

              in  λ(m : List Entry) →
                    value.dictionary
                      ( List/map
                          Entry
                          { mapKey : Preserves, mapValue : Preserves }
                          (λ(e : Entry) → e with mapKey = value.string e.mapKey)
                          m
                      )
          , string = value.string
          }

in  fromJSON
