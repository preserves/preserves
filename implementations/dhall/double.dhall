{-|
Create a Preserves floating-point value from a `Double` value
-}
let Preserves = ./Type.dhall

let Preserves/function = ./function.dhall

let double
    : Double → Preserves
    = λ(x : Double) →
      λ(Preserves : Type) →
      λ(value : Preserves/function Preserves) →
        value.double x

in  double
