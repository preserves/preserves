{-|
Create a Preserves boolean map from a `Bool` value
-}
let Preserves/Type = ./Type.dhall

let Preserves/function = ./function.dhall

let bool
    : Bool → Preserves/Type
    = λ(x : Bool) →
      λ(Preserves : Type) →
      λ(value : Preserves/function Preserves) →
        value.boolean x

in  bool
