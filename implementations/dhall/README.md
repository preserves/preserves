# Dhall

Not a true implementation of Preserves, but functions for translating Dhall
values to Preserves and rendering them.

For example, to generate configuration for a Syndicate server listener:
```dhall
let Prelude = ./Prelude.dhall

let Preserves = ./package.dhall

let Tcp/Type = { address : Text, port : Natural }

let RelayListener/Type = { transport : Tcp/Type }

let RequireService/Type = { relayListener : RelayListener/Type }

let Tcp/toPreserves =
      λ(tcp : Tcp/Type) →
        Preserves.record
          (Preserves.symbol "tcp")
          [ Preserves.string tcp.address
          , Preserves.integer (Prelude.Natural.toInteger tcp.port)
          ]

let RelayListener/toPreserves =
      λ(relayListener : RelayListener/Type) →
        Preserves.record
          (Preserves.symbol "relay-listener")
          [ Tcp.toPreserves relayListener.transport ]

let RequireService/toPreserves =
      λ(requireService : RequireService/Type) →
        Preserves.record
          (Preserves.symbol "require-service")
          [ RelayListener.toPreserves requireService.relayListener ]

let example = { relayListener.transport = { address = "127.0.0.1", port = 1 } }

let rendering = Preserves.render (RequireService.toPreserves example)

let check =
        assert
      : rendering ≡ "<require-service <relay-listener <tcp \"127.0.0.1\" 1>>>"

in  rendering

```
