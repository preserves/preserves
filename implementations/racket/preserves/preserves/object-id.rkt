#lang racket/base

(provide object-id)

(define table (make-weak-hasheq))
(define next 0)

(define (object-id x)
  (hash-ref! table x (lambda () (let ((v next)) (set! next (+ v 1)) v))))
