# Split out to separate repository

The Rust implementation of Preserves has been split out into a separate git repository,
<https://gitlab.com/preserves/preserves-rs>.

The final released versions that were here were

 - `preserves` v4.992.2,
 - `preserves-schema` v5.992.0,
 - `preserves-tools` v4.992.2, and
 - `preserves-path` v5.992.0.

Subsequent releases live in the other repository.
