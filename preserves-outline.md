---
no_site_title: true
title: "Preserves: Outline Syntax"
---

Tony Garnock-Jones <tonyg@leastfixedpoint.com>  
October 2024. Version 0.0.1.

  [text syntax]: preserves-text.html
  [abnf]: https://tools.ietf.org/html/rfc7405

*Preserves* is a data model, with associated serialization formats. This
document defines one of those formats: an indentation-sensitive *outline
syntax*, inspired by [YAML](https://yaml.org/), for quasi-tabular
presentation of `Dictionary` and `Sequence` values from the [Preserves data
model](preserves.html) that is easy for people to read and write. It
extends the ordinary [human-readable text syntax][text syntax].

Outline syntax is primarily intended for writing documents by hand. It
includes a number of features designed to reduce the possibility of error
for human-authored documents. However, it is perfectly possible to
mechanically produce documents using outline syntax, effectively treating
it as a transfer syntax; implementation notes in the text highlight areas
where special care is needed.

## Preliminaries

The outline syntax grammar includes by reference the definition of `Value`
from the [text syntax][], as well as the definitions that `Value` depends
on.

**ABNF.** The definition uses [case-sensitive ABNF][abnf] with ordered choice.
ABNF allows easy definition of US-ASCII-based languages. However,
Preserves is a Unicode-based language. Therefore, we reinterpret ABNF as
a grammar for recognising sequences of Unicode scalar values.
In addition, we augment ABNF nonterminals with parameters and arguments
using braces. Parameters are used to represent indentation context.

<a id="encoding"></a>
**Encoding.** Outline syntax *SHOULD* be encoded using UTF-8 where
possible.

<a id="whitespace"></a>
**Whitespace and indentation.** Outline syntax is indentation sensitive and
line-oriented. Horizonal whitespace `hws` is defined as any number of
spaces or tabs. A newline `nl` is horizonal whitespace followed by an
optional carriage return and a mandatory line feed.

               hws = *(%x20 / %x09)
                nl = hws [CR] LF

An `Indent{n}` is a series of one or more `nl` followed by exactly *n*
literal spaces. Tabs are not allowed for indentation.

         Indent{n} = 1*nl n%x20

## Grammar

A `Document` is a single outlined value, with optional additional
uninterpreted text.[^sequence-of-documents]
[^uninterpreted-additional-text]

          Document = Outline{0} [nl "---" nl *<any unicode scalar value>]

[^sequence-of-documents]: In some cases, applications may choose to
    interpret the additional text as another `Document`, yielding a
    sequence of outlined values.

[^uninterpreted-additional-text]: **Implementation note.** While the
    grammar as written accepts only text after an end-of-outline mark,
    implementations may wish to generalize this to "see through" the
    encoding of the text and accept arbitrary binary additional data.

An `Outline{n}` is either a single `Value`, a block-quoted `String`, a
`Dictionary` of key-value pairs, a `Sequence`, or a plain `String`.

        Outline{n} = [Indent{n}] hws SimpleValue
                   / BlockQuote{n}
                   / OutlineDictionary{n}
                   / OutlineSequence{n}
                   / PlainString

A `SimpleValue` is a `Value` that cannot be mistaken for a `PlainString`.

       SimpleValue = Record / Collection /
                     Boolean / String / ByteString / QuotedSymbol /
                     Double / SignedInteger /
                     Embedded

A `BlockQuote{n}` is *either* a one-line string preceded by `>`, *or* a
sequence of *n*-indented lines, each preceded by `>`.

     BlockQuote{n} = hws ">" Line
                   / *(Indent{n} ">" Line)
              Line = *<any unicode scalar except CR or LF>

No whitespace is stripped from the `Line`s following each `>`. However, the
`CR` or `LF` at the end of each `Line` is not included in the parsed
`String`. When a `BlockQuote{n}` is written as a sequence of indented
lines, the parsed `String` is comprised of those lines joined together,
separated by a single `LF`.

An `OutlineDictionary{n}` is one or more key-value pairs.

    OutlineDictionary{n} = 1*(Indent{n} SimpleValue hws ":" Outline{m > n})

Each key is *n*-indented, and the corresponding value is *m*-indented for
some *m* strictly larger than *n*. Each key-value pair may have a different
*m*.

An `OutlineSequence{n}` is one or more outlined values, each preceded by a
dash.

    OutlineSequence{n} = 1*(Indent{n} "-" Outline{m > n})

Each value is *m*-indented for some *m* strictly larger than *n*. Each
value may have a different *m*. To avoid confusion involving negative
numbers, implementations *SHOULD* print a space after a dash whenever a
`SimpleValue` will follow on the same line.

A `PlainString` cannot contain carriage returns or newlines. Leading
horizontal whitespace is stripped. It can otherwise contain arbitrary text.

       PlainString = hws *<any unicode scalar except CR or LF>

Because outline syntax is intended to be written by hand, we forbid
`PlainString` values "`true`", "`false`" and "`null`" from appearing.
Implementations *MUST* reject these `PlainString` inputs and *MUST NOT*
produce them.

Similarly, a `String` that have a prefix that looks like a `Double` or a
`SignedInteger` *MUST NOT* be expressed as a `PlainString`. If
`PlainString` syntax were used for such a `String`, it would be parsed back
as a number, not as a string. ((TODO: Is this quite right? Can we arrange
things so that only strings that also match `Number` are confusable, rather
than the having-a-prefix thing?))

## <a id="annotations"></a>Annotations and Comments

TODO

**Interpreter specification lines.** Annotate the top-level value and are elsewhere not accepted

## Examples

 - -

   ```
   hello world
   ```

   `"hello world"`

 - -

   ```
   >It is a truth
   >universally acknowledged
   ```

   `"It is a truth\nuniversally acknowledged"`

 - -

   ```
   - 1
   ```

   `[1]`

 - -

   ```
   -1
   ```

   `-1`

 - -

   ```
   - 1
   - 2
   - 3
   ```

   `[1 2 3]`

 - -

   ```
   a: 1
   b: 2
   c: 3
   ```

   `{a: 1, b: 2, c: 3}`

 - -

   ```
   -
   -
     >It is a truth
     >universally acknowledged
   - > x
   -   y
   ```

   `["", "It is a truth\nuniversally acknowledged", " x", "y"]`

 - -

   ```
   [1 2]: 3
   [one two]: >3
   4: 'hi'
   z:
    -a
    -b
   w:
     c:m
     d:n
   "x": y
   ```

   `{[1 2]: 3, [one two]: "3", 4: hi, z: ["a" "b"], w: {c: "m", d: "n"}, "x": "y"}`

 - -

   ```
   -
     - 1
     - 2
   -
     - 3
     - 4
   ```

   `[[1 2] [3 4]]`

 - -

   ```
   -
   ```

   `[""]`

 - -

   ```
   '-'
   ```

   `-`

 - -

   ```
   >-
   ```

   `"-"`

 - -

   ```
   "-"
   ```

   `"="`

<!-- Heading to visually offset the footnotes from the main document: -->
## Notes
