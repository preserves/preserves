---
title: Change History
body_class: no-counters
---

## Version 0.996.1, February 2025

Addition of [media types and file extensions](preserves-media-types.html) for use with
Preserves data.

## Version 0.996.0, September 2024

Text syntax:

 - Switch from `|`...`|` to `'`...`'` quotation of `Symbol`s. The syntax with pipes originated
   with Racket and other Schemes and Lisps, but is unusual outside the Lisp world. The syntax
   with single-quotes lines up with languages like Smalltalk, Erlang, Prolog and Ruby.

## Version 0.995.0, May 2024

Text syntax:

 - Support Unix interpreter specification (`#!`) lines, reading them as annotations.

Schema:

 - Document pattern extensibility: essentially, prefix-matching of sequence-like `Value`s.

## Version 0.994.0, March 2024

Overall:

 - Remove `Float`, leaving `Double` as the sole supported kind of IEEE 754 floating-point number.

Binary syntax:

 - Remove `0x87 0x04` syntax for `Float`s.

Text syntax:

 - Switch `#!` to `#:` as the prefix for `Embedded` values.
 - Remove trailing-`f` syntax for `Float`s.

Schema:

 - Clarify the idea of "sufficiently identifierlike" values.
 - Give a schema, `host.prs`, describing host-language types abstractly.

P-expressions:

 - Introduce `Trailer`s, for annotations at the end of a block.

## Version 0.992.0, November 2023

Text syntax:

 - Remove commas from whitespace. Instead, explicitly ignore them inside `Sequence`s, `Set`s,
   and `Dictionary`s.

## Version 0.991.0, October 2023

Overall:

 - Introduce P-expressions.
 - Clarify the meaning of lexicographical comparison in Preserves.

Text syntax:

 - Use `#` instead of `;` to mark line comments.

## Version 0.990.1, October 2023

Overall:

 - Repair examples of binary encoding to actually match the current specification.

Text syntax:

 - Introduce the notion of "delimiters".
 - Make use of standard EBNF definitions like `DQUOTE`, `ALPHA` and `DIGIT`.

Schema:

 - Relax the placement of `/` in `OrPattern`s to match the actual implementations.
 - Relax the placement of `&` in `AndPattern`s to match the actual implementations.

## Version 0.990.0, October 2023

Overall:

 - Include "cheat sheet" quick-reference cards for Preserves syntax.

Binary syntax:

 - Remove `0xA0`-`0xAF` and `0x90`-`0x9F` tags, which were used for various compact
   representations of `SignedInteger`s. Now, all integers use tag `0xB0`.
 - Switch from tags `0x82` and `0x83` for `Float` and `Double`, respectively, to tag `0x87`
   plus a length in bytes for both.

## Version 0.7.1, October 2023

Overall:

 - Update metaconventions and examples in `conventions.md`
 - Make clear throughout that Preserves works with Unicode *scalars*, not *code points*.
 - Clarify that unpaired surrogate code points are not permitted in strings and symbols.

Model:

 - Add note regarding JSON compatibility and unpaired surrogate code points.
 - Add appendix describing the "merge" of two `Value`s.

Text syntax:

 - Include recommendation for implementations to print sets and dictionaries in
   Preserves-ordering.
 - Include recommendation that URL-safe base64 should be emitted by default.

## Version 0.6.4, February 2023

Text syntax:

 - Alter parsing of numbers and symbols to use `SymbolOrNumber` plus a disambiguation step.

Schema:

 - Place "smaller" definitions first in alternations. For example, `EmbeddedTypeName` now has
   the `#f` branch first, rather than the `Ref` branch.

## Version 0.6.3, June 2022

Overall:

 - Split monolithic specification document into smaller, more focussed pieces.

Model:

 - Clarify that `NUL` characters (code point zero) are permitted in strings and symbols.

Text syntax:

 - Clarify that annotations on a value are ordered.

Schema:

 - Include descriptions of host-language types.
 - Include the beginnings of semantics.
