---
projectpages: "https://gitlab.com/preserves/preserves"
projecttree: "https://gitlab.com/preserves/preserves/tree/main"
title: "Preserves: an Expressive Data Language"
no_site_title: true
---

This [repository]({{page.projectpages}}) contains a
[definition](preserves.html) and various implementations of *Preserves*, a
data model with associated serialization formats in many ways
comparable to JSON, XML, S-expressions, CBOR, ASN.1 BER, and so on.

## Core documents

### Preserves data model and serialization formats

Preserves is defined in terms of a syntax-neutral
[data model and semantics](preserves.html#semantics)
which all transfer syntaxes share. This allows trivial, completely
automatic, perfect-fidelity conversion between syntaxes.

 - [Preserves specification](preserves.html):
    - [Preserves semantics and data model](preserves.html#semantics),
    - [Preserves textual syntax](preserves-text.html), and
    - [Preserves machine-oriented binary syntax](preserves-binary.html)
 - [Preserves tutorial](TUTORIAL.html)
 - [Quick Reference for Preserves syntax](cheatsheet.html)
 - [Canonical Form for Binary Syntax](canonical-binary.html)
 - [Syrup](https://github.com/ocapn/syrup#pseudo-specification), a
   hybrid binary/human-readable syntax for the Preserves data model
 - [Media Types (MIME types) and File Extensions](preserves-media-types.html)

### Preserves schema and queries

 - [Preserves Schema specification](preserves-schema.html)
 - [Preserves Path specification](preserves-path.html)

## Implementations

#### Implementations of the data model, plus Preserves textual and binary transfer syntax

| Language[^pre-alpha-implementations] | Code                                                                         | Package                                                                        | Docs                                      |
|--------------------------------------|------------------------------------------------------------------------------|--------------------------------------------------------------------------------|-------------------------------------------|
| Nim                                  | [git.syndicate-lang.org](https://git.syndicate-lang.org/ehmry/preserves-nim) |                                                                                |                                           |
| Python                               | [preserves.dev]({{page.projecttree}}/implementations/python/)                | [`pip install preserves`](https://pypi.org/project/preserves/)                 | [docs](python/latest/)                    |
| Racket                               | [preserves.dev]({{page.projecttree}}/implementations/racket/preserves/)      | [`raco pkg install preserves`](https://pkgs.racket-lang.org/package/preserves) |                                           |
| Rust                                 | [preserves.dev](https://gitlab.com/preserves/preserves-rs/)                  | [`cargo add preserves`](https://crates.io/crates/preserves)                    | [docs](https://docs.rs/preserves/latest/) |
| Squeak Smalltalk                     | [SqueakSource](https://squeaksource.com/Preserves.html)                      | `Installer ss project: 'Preserves';`<br>`  install: 'Preserves'`               |                                           |
| TypeScript/JavaScript                | [preserves.dev]({{page.projecttree}}/implementations/javascript/)            | [`yarn add @preserves/core`](https://www.npmjs.com/package/@preserves/core)    |                                           |

[^pre-alpha-implementations]: Pre-alpha implementations also exist for
    [C]({{page.projecttree}}/implementations/c/) and
    [C++]({{page.projecttree}}/implementations/cpp/).

#### Implementations of the data model, plus Syrup transfer syntax

| Language   | Code                                                                                                                             |
|------------|----------------------------------------------------------------------------------------------------------------------------------|
| Guile      | [github.com/ocapn/syrup](https://github.com/ocapn/syrup/blob/master/impls/guile/syrup.scm)                                       |
| Haskell    | [github.com/zenhack/haskell-preserves](https://github.com/zenhack/haskell-preserves)                                             |
| JavaScript | [github.com/zarutian/agoric-sdk](https://github.com/zarutian/agoric-sdk/blob/zarutian/captp_variant/packages/captp/lib/syrup.js) |
| Python     | [github.com/ocapn/syrup](https://github.com/ocapn/syrup/blob/master/impls/python/syrup.py)                                       |
| Racket     | [github.com/ocapn/syrup](https://github.com/ocapn/syrup/blob/master/impls/racket/syrup/syrup.rkt)                                |

## Tools

### Preserves documents

 - [preserves-tool](doc/preserves-tool.html), generic syntax translation and pretty-printing

### Preserves Schema documents and codegen

 - [Tools for working with Preserves Schema](doc/schema-tools.html)

## Additional resources

 - [Change History](changes.html) of the Preserves specifications
 - [Preserves Expressions (P-expressions, pexprs)](preserves-expressions.html)
 - Some [conventions for common data types](conventions.html)
 - [Open questions](questions.html); see also the
   [issues list]({{page.projectpages}}/issues)
 - [Why not Just Use JSON?](why-not-json.html)

## Contact

Tony Garnock-Jones <tonyg@leastfixedpoint.com>

## Licensing

The contents of this repository are made available to you under the
[Apache License, version 2.0](LICENSE)
(<http://www.apache.org/licenses/LICENSE-2.0>), and are Copyright
2018-2024 Tony Garnock-Jones.

## Notes
