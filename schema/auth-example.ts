////------------------------------------------------------------
//// auth-example.ts

import * as _ from "@preserves/core";

export const $authenticated = _.Symbol.for("authenticated");
export const $none = _.Symbol.for("none");
export const $password = _.Symbol.for("password");
export const $publickey = _.Symbol.for("publickey");
export const __lit1 = Uint8Array.from([110, 111, 110, 101]);
export const __lit10 = _.Symbol.for("ed25519-private-key");
export const __lit2 = Uint8Array.from([112, 117, 98, 108, 105, 99, 107, 101, 121]);
export const __lit3 = Uint8Array.from([112, 97, 115, 115, 119, 111, 114, 100]);
export const __lit7 = _.Symbol.for("authentication-method-acceptable");
export const __lit8 = _.Symbol.for("authentication-acceptable?");
export const __lit9 = _.Symbol.for("ed25519-public-key");

let __schema: _.Value | null = null;

export function _schema() {
    if (__schema === null) {
        __schema = _.decode<_.GenericEmbedded>(_.Bytes.fromHex("b4b306736368656d61b7b30776657273696f6eb00101b30b646566696e6974696f6e73b7b3095075626c69634b6579b4b303726566b584b310456432353531395075626c69634b657984b30d537368417574684d6574686f64b4b3026f72b5b5b1046e6f6e65b4b3036c6974b2046e6f6e658484b5b1097075626c69636b6579b4b3036c6974b2097075626c69636b65798484b5b10870617373776f7264b4b3036c6974b20870617373776f726484848484b30e5373684175746852657175657374b4b3026f72b5b5b1046e6f6e65b4b303726563b4b3036c6974b3046e6f6e6584b4b3057475706c65b5b4b3056e616d6564b308757365726e616d65b4b30461746f6db306537472696e67848484848484b5b1097075626c69636b6579b4b303726563b4b3036c6974b3097075626c69636b657984b4b3057475706c65b5b4b3056e616d6564b308757365726e616d65b4b30461746f6db306537472696e678484b4b3056e616d6564b3036b6579b4b303726566b584b3095075626c69634b6579848484848484b5b10870617373776f7264b4b303726563b4b3036c6974b30870617373776f726484b4b3057475706c65b5b4b3056e616d6564b308757365726e616d65b4b30461746f6db306537472696e678484b4b3056e616d6564b30870617373776f7264b4b30461746f6db306537472696e678484848484848484b310456432353531395075626c69634b6579b4b303726563b4b3036c6974b312656432353531392d7075626c69632d6b657984b4b3057475706c65b5b4b3056e616d6564b30171b4b30461746f6db30a42797465537472696e678484848484b31145643235353139507269766174654b6579b4b303726563b4b3036c6974b313656432353531392d707269766174652d6b657984b4b3057475706c65b5b4b3056e616d6564b30171b4b30461746f6db30a42797465537472696e678484b4b3056e616d6564b30164b4b30461746f6db30a42797465537472696e678484848484b31453736841757468656e7469636174656455736572b4b303726563b4b3036c6974b30d61757468656e7469636174656484b4b3057475706c65b5b4b3056e616d6564b308757365726e616d65b4b30461746f6db306537472696e678484b4b3056e616d6564b30773657276696365b4b30461746f6db30a42797465537472696e678484848484b31b53736841757468656e7469636174696f6e41636365707461626c65b4b303726563b4b3036c6974b31a61757468656e7469636174696f6e2d61636365707461626c653f84b4b3057475706c65b5b4b3056e616d6564b3066d6574686f64b4b303726566b584b30d537368417574684d6574686f648484b4b3056e616d6564b30772657175657374b4b303726566b584b30e53736841757468526571756573748484b4b3056e616d6564b3026f6bb4b30461746f6db307426f6f6c65616e8484848484b32153736841757468656e7469636174696f6e4d6574686f6441636365707461626c65b4b303726563b4b3036c6974b32061757468656e7469636174696f6e2d6d6574686f642d61636365707461626c6584b4b3057475706c65b5b4b3056e616d6564b3066d6574686f64b4b303726566b584b30d537368417574684d6574686f64848484848484b30c656d62656464656454797065808484"));
    };
    return __schema;
}

export const _imports = {}


export type SshAuthenticatedUser = (
    {"username": string, "service": _.Bytes} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type SshAuthMethod = (
    ({"_variant": "none"} | {"_variant": "publickey"} | {"_variant": "password"}) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type SshAuthRequest = (
    (
        {"_variant": "none", "username": string} |
        {"_variant": "publickey", "username": string, "key": PublicKey} |
        {"_variant": "password", "username": string, "password": string}
    ) &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type SshAuthenticationMethodAcceptable = (
    {"method": SshAuthMethod} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type SshAuthenticationAcceptable = (
    {"method": SshAuthMethod, "request": SshAuthRequest, "ok": boolean} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type PublicKey = Ed25519PublicKey;

export type Ed25519PublicKey = (
    {"q": _.Bytes} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Ed25519PrivateKey = (
    {"q": _.Bytes, "d": _.Bytes} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);


export function SshAuthenticatedUser({username, service}: {username: string, service: _.Bytes}): SshAuthenticatedUser {
    return {
        "username": username,
        "service": service,
        __as_preserve__() {return fromSshAuthenticatedUser(this);},
        __preserve_on__(e) { e.push(fromSshAuthenticatedUser(this)); },
        __preserve_text_on__(w) { w.push(fromSshAuthenticatedUser(this)); }
    };
}

SshAuthenticatedUser.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("SshAuthenticatedUser")
    };
}

export namespace SshAuthMethod {
    export function none(): SshAuthMethod {
        return {
            "_variant": "none",
            __as_preserve__() {return fromSshAuthMethod(this);},
            __preserve_on__(e) { e.push(fromSshAuthMethod(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthMethod(this)); }
        };
    };
    none.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SshAuthMethod"),
            variant: _.Symbol.for("none")
        };
    };
    export function publickey(): SshAuthMethod {
        return {
            "_variant": "publickey",
            __as_preserve__() {return fromSshAuthMethod(this);},
            __preserve_on__(e) { e.push(fromSshAuthMethod(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthMethod(this)); }
        };
    };
    publickey.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SshAuthMethod"),
            variant: _.Symbol.for("publickey")
        };
    };
    export function password(): SshAuthMethod {
        return {
            "_variant": "password",
            __as_preserve__() {return fromSshAuthMethod(this);},
            __preserve_on__(e) { e.push(fromSshAuthMethod(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthMethod(this)); }
        };
    };
    password.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SshAuthMethod"),
            variant: _.Symbol.for("password")
        };
    };
}

export namespace SshAuthRequest {
    export function none(username: string): SshAuthRequest {
        return {
            "_variant": "none",
            "username": username,
            __as_preserve__() {return fromSshAuthRequest(this);},
            __preserve_on__(e) { e.push(fromSshAuthRequest(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthRequest(this)); }
        };
    };
    none.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SshAuthRequest"),
            variant: _.Symbol.for("none")
        };
    };
    export function publickey({username, key}: {username: string, key: PublicKey}): SshAuthRequest {
        return {
            "_variant": "publickey",
            "username": username,
            "key": key,
            __as_preserve__() {return fromSshAuthRequest(this);},
            __preserve_on__(e) { e.push(fromSshAuthRequest(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthRequest(this)); }
        };
    };
    publickey.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SshAuthRequest"),
            variant: _.Symbol.for("publickey")
        };
    };
    export function password({username, password}: {username: string, password: string}): SshAuthRequest {
        return {
            "_variant": "password",
            "username": username,
            "password": password,
            __as_preserve__() {return fromSshAuthRequest(this);},
            __preserve_on__(e) { e.push(fromSshAuthRequest(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthRequest(this)); }
        };
    };
    password.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("SshAuthRequest"),
            variant: _.Symbol.for("password")
        };
    };
}

export function SshAuthenticationMethodAcceptable(method: SshAuthMethod): SshAuthenticationMethodAcceptable {
    return {
        "method": method,
        __as_preserve__() {return fromSshAuthenticationMethodAcceptable(this);},
        __preserve_on__(e) { e.push(fromSshAuthenticationMethodAcceptable(this)); },
        __preserve_text_on__(w) { w.push(fromSshAuthenticationMethodAcceptable(this)); }
    };
}

SshAuthenticationMethodAcceptable.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("SshAuthenticationMethodAcceptable")
    };
}

export function SshAuthenticationAcceptable(
    {method, request, ok}: {method: SshAuthMethod, request: SshAuthRequest, ok: boolean}
): SshAuthenticationAcceptable {
    return {
        "method": method,
        "request": request,
        "ok": ok,
        __as_preserve__() {return fromSshAuthenticationAcceptable(this);},
        __preserve_on__(e) { e.push(fromSshAuthenticationAcceptable(this)); },
        __preserve_text_on__(w) { w.push(fromSshAuthenticationAcceptable(this)); }
    };
}

SshAuthenticationAcceptable.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("SshAuthenticationAcceptable")
    };
}

export function PublicKey(value: Ed25519PublicKey): PublicKey {return value;}

PublicKey.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("PublicKey")
    };
}

export function Ed25519PublicKey(q: _.Bytes): Ed25519PublicKey {
    return {
        "q": q,
        __as_preserve__() {return fromEd25519PublicKey(this);},
        __preserve_on__(e) { e.push(fromEd25519PublicKey(this)); },
        __preserve_text_on__(w) { w.push(fromEd25519PublicKey(this)); }
    };
}

Ed25519PublicKey.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("Ed25519PublicKey")
    };
}

export function Ed25519PrivateKey({q, d}: {q: _.Bytes, d: _.Bytes}): Ed25519PrivateKey {
    return {
        "q": q,
        "d": d,
        __as_preserve__() {return fromEd25519PrivateKey(this);},
        __preserve_on__(e) { e.push(fromEd25519PrivateKey(this)); },
        __preserve_text_on__(w) { w.push(fromEd25519PrivateKey(this)); }
    };
}

Ed25519PrivateKey.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("Ed25519PrivateKey")
    };
}

export function asSshAuthenticatedUser<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): SshAuthenticatedUser {
    let result = toSshAuthenticatedUser(v);
    if (result === void 0) throw new TypeError(`Invalid SshAuthenticatedUser: ${_.stringify(v)}`);
    return result;
}

export function toSshAuthenticatedUser<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SshAuthenticatedUser {
    let result: undefined | SshAuthenticatedUser;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $authenticated) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_.Bytes) | undefined;
                _tmp2 = _.Bytes.isBytes(v[1]) ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    result = {
                        "username": _tmp1,
                        "service": _tmp2,
                        __as_preserve__() {return fromSshAuthenticatedUser(this);},
                        __preserve_on__(e) { e.push(fromSshAuthenticatedUser(this)); },
                        __preserve_text_on__(w) { w.push(fromSshAuthenticatedUser(this)); }
                    };
                };
            };
        };
    };
    return result;
}

SshAuthenticatedUser.__from_preserve__ = toSshAuthenticatedUser;

export function fromSshAuthenticatedUser<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: SshAuthenticatedUser): _.Value<_embedded> {return _.Record($authenticated, [_v["username"], _v["service"]]);}

export function asSshAuthMethod<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): SshAuthMethod {
    let result = toSshAuthMethod(v);
    if (result === void 0) throw new TypeError(`Invalid SshAuthMethod: ${_.stringify(v)}`);
    return result;
}

export function toSshAuthMethod<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SshAuthMethod {
    let _tmp0: ({}) | undefined;
    let result: undefined | SshAuthMethod;
    _tmp0 = _.is(v, __lit1) ? {} : void 0;
    if (_tmp0 !== void 0) {
        result = {
            "_variant": "none",
            __as_preserve__() {return fromSshAuthMethod(this);},
            __preserve_on__(e) { e.push(fromSshAuthMethod(this)); },
            __preserve_text_on__(w) { w.push(fromSshAuthMethod(this)); }
        };
    };
    if (result === void 0) {
        let _tmp1: ({}) | undefined;
        _tmp1 = _.is(v, __lit2) ? {} : void 0;
        if (_tmp1 !== void 0) {
            result = {
                "_variant": "publickey",
                __as_preserve__() {return fromSshAuthMethod(this);},
                __preserve_on__(e) { e.push(fromSshAuthMethod(this)); },
                __preserve_text_on__(w) { w.push(fromSshAuthMethod(this)); }
            };
        };
        if (result === void 0) {
            let _tmp2: ({}) | undefined;
            _tmp2 = _.is(v, __lit3) ? {} : void 0;
            if (_tmp2 !== void 0) {
                result = {
                    "_variant": "password",
                    __as_preserve__() {return fromSshAuthMethod(this);},
                    __preserve_on__(e) { e.push(fromSshAuthMethod(this)); },
                    __preserve_text_on__(w) { w.push(fromSshAuthMethod(this)); }
                };
            };
        };
    };
    return result;
}

export namespace SshAuthMethod {export const __from_preserve__ = toSshAuthMethod;}

export function fromSshAuthMethod<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: SshAuthMethod): _.Value<_embedded> {
    switch (_v._variant) {
        case "none": {return __lit1;};
        case "publickey": {return __lit2;};
        case "password": {return __lit3;};
    };
}

export function asSshAuthRequest<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): SshAuthRequest {
    let result = toSshAuthRequest(v);
    if (result === void 0) throw new TypeError(`Invalid SshAuthRequest: ${_.stringify(v)}`);
    return result;
}

export function toSshAuthRequest<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SshAuthRequest {
    let result: undefined | SshAuthRequest;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $none) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                result = {
                    "_variant": "none",
                    "username": _tmp1,
                    __as_preserve__() {return fromSshAuthRequest(this);},
                    __preserve_on__(e) { e.push(fromSshAuthRequest(this)); },
                    __preserve_text_on__(w) { w.push(fromSshAuthRequest(this)); }
                };
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp2: ({}) | undefined;
            _tmp2 = _.is(v.label, $publickey) ? {} : void 0;
            if (_tmp2 !== void 0) {
                let _tmp3: (string) | undefined;
                _tmp3 = typeof v[0] === 'string' ? v[0] : void 0;
                if (_tmp3 !== void 0) {
                    let _tmp4: (PublicKey) | undefined;
                    _tmp4 = toPublicKey(v[1]);
                    if (_tmp4 !== void 0) {
                        result = {
                            "_variant": "publickey",
                            "username": _tmp3,
                            "key": _tmp4,
                            __as_preserve__() {return fromSshAuthRequest(this);},
                            __preserve_on__(e) { e.push(fromSshAuthRequest(this)); },
                            __preserve_text_on__(w) { w.push(fromSshAuthRequest(this)); }
                        };
                    };
                };
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp5: ({}) | undefined;
                _tmp5 = _.is(v.label, $password) ? {} : void 0;
                if (_tmp5 !== void 0) {
                    let _tmp6: (string) | undefined;
                    _tmp6 = typeof v[0] === 'string' ? v[0] : void 0;
                    if (_tmp6 !== void 0) {
                        let _tmp7: (string) | undefined;
                        _tmp7 = typeof v[1] === 'string' ? v[1] : void 0;
                        if (_tmp7 !== void 0) {
                            result = {
                                "_variant": "password",
                                "username": _tmp6,
                                "password": _tmp7,
                                __as_preserve__() {return fromSshAuthRequest(this);},
                                __preserve_on__(e) { e.push(fromSshAuthRequest(this)); },
                                __preserve_text_on__(w) { w.push(fromSshAuthRequest(this)); }
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export namespace SshAuthRequest {export const __from_preserve__ = toSshAuthRequest;}

export function fromSshAuthRequest<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: SshAuthRequest): _.Value<_embedded> {
    switch (_v._variant) {
        case "none": {return _.Record($none, [_v["username"]]);};
        case "publickey": {
            return _.Record($publickey, [_v["username"], fromPublicKey<_embedded>(_v["key"])]);
        };
        case "password": {return _.Record($password, [_v["username"], _v["password"]]);};
    };
}

export function asSshAuthenticationMethodAcceptable<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): SshAuthenticationMethodAcceptable {
    let result = toSshAuthenticationMethodAcceptable(v);
    if (result === void 0) throw new TypeError(`Invalid SshAuthenticationMethodAcceptable: ${_.stringify(v)}`);
    return result;
}

export function toSshAuthenticationMethodAcceptable<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SshAuthenticationMethodAcceptable {
    let result: undefined | SshAuthenticationMethodAcceptable;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, __lit7) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (SshAuthMethod) | undefined;
            _tmp1 = toSshAuthMethod(v[0]);
            if (_tmp1 !== void 0) {
                result = {
                    "method": _tmp1,
                    __as_preserve__() {return fromSshAuthenticationMethodAcceptable(this);},
                    __preserve_on__(e) { e.push(fromSshAuthenticationMethodAcceptable(this)); },
                    __preserve_text_on__(w) { w.push(fromSshAuthenticationMethodAcceptable(this)); }
                };
            };
        };
    };
    return result;
}

SshAuthenticationMethodAcceptable.__from_preserve__ = toSshAuthenticationMethodAcceptable;

export function fromSshAuthenticationMethodAcceptable<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: SshAuthenticationMethodAcceptable): _.Value<_embedded> {return _.Record(__lit7, [fromSshAuthMethod<_embedded>(_v["method"])]);}

export function asSshAuthenticationAcceptable<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): SshAuthenticationAcceptable {
    let result = toSshAuthenticationAcceptable(v);
    if (result === void 0) throw new TypeError(`Invalid SshAuthenticationAcceptable: ${_.stringify(v)}`);
    return result;
}

export function toSshAuthenticationAcceptable<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SshAuthenticationAcceptable {
    let result: undefined | SshAuthenticationAcceptable;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, __lit8) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (SshAuthMethod) | undefined;
            _tmp1 = toSshAuthMethod(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (SshAuthRequest) | undefined;
                _tmp2 = toSshAuthRequest(v[1]);
                if (_tmp2 !== void 0) {
                    let _tmp3: (boolean) | undefined;
                    _tmp3 = typeof v[2] === 'boolean' ? v[2] : void 0;
                    if (_tmp3 !== void 0) {
                        result = {
                            "method": _tmp1,
                            "request": _tmp2,
                            "ok": _tmp3,
                            __as_preserve__() {return fromSshAuthenticationAcceptable(this);},
                            __preserve_on__(e) { e.push(fromSshAuthenticationAcceptable(this)); },
                            __preserve_text_on__(w) { w.push(fromSshAuthenticationAcceptable(this)); }
                        };
                    };
                };
            };
        };
    };
    return result;
}

SshAuthenticationAcceptable.__from_preserve__ = toSshAuthenticationAcceptable;

export function fromSshAuthenticationAcceptable<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: SshAuthenticationAcceptable): _.Value<_embedded> {
    return _.Record(
        __lit8,
        [
            fromSshAuthMethod<_embedded>(_v["method"]),
            fromSshAuthRequest<_embedded>(_v["request"]),
            _v["ok"]
        ]
    );
}

export function asPublicKey<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): PublicKey {
    let result = toPublicKey(v);
    if (result === void 0) throw new TypeError(`Invalid PublicKey: ${_.stringify(v)}`);
    return result;
}

export function toPublicKey<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | PublicKey {
    let _tmp0: (Ed25519PublicKey) | undefined;
    let result: undefined | PublicKey;
    _tmp0 = toEd25519PublicKey(v);
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

PublicKey.__from_preserve__ = toPublicKey;

export function fromPublicKey<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: PublicKey): _.Value<_embedded> {return fromEd25519PublicKey<_embedded>(_v);}

export function asEd25519PublicKey<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Ed25519PublicKey {
    let result = toEd25519PublicKey(v);
    if (result === void 0) throw new TypeError(`Invalid Ed25519PublicKey: ${_.stringify(v)}`);
    return result;
}

export function toEd25519PublicKey<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Ed25519PublicKey {
    let result: undefined | Ed25519PublicKey;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, __lit9) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Bytes) | undefined;
            _tmp1 = _.Bytes.isBytes(v[0]) ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                result = {
                    "q": _tmp1,
                    __as_preserve__() {return fromEd25519PublicKey(this);},
                    __preserve_on__(e) { e.push(fromEd25519PublicKey(this)); },
                    __preserve_text_on__(w) { w.push(fromEd25519PublicKey(this)); }
                };
            };
        };
    };
    return result;
}

Ed25519PublicKey.__from_preserve__ = toEd25519PublicKey;

export function fromEd25519PublicKey<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Ed25519PublicKey): _.Value<_embedded> {return _.Record(__lit9, [_v["q"]]);}

export function asEd25519PrivateKey<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Ed25519PrivateKey {
    let result = toEd25519PrivateKey(v);
    if (result === void 0) throw new TypeError(`Invalid Ed25519PrivateKey: ${_.stringify(v)}`);
    return result;
}

export function toEd25519PrivateKey<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Ed25519PrivateKey {
    let result: undefined | Ed25519PrivateKey;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, __lit10) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Bytes) | undefined;
            _tmp1 = _.Bytes.isBytes(v[0]) ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_.Bytes) | undefined;
                _tmp2 = _.Bytes.isBytes(v[1]) ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    result = {
                        "q": _tmp1,
                        "d": _tmp2,
                        __as_preserve__() {return fromEd25519PrivateKey(this);},
                        __preserve_on__(e) { e.push(fromEd25519PrivateKey(this)); },
                        __preserve_text_on__(w) { w.push(fromEd25519PrivateKey(this)); }
                    };
                };
            };
        };
    };
    return result;
}

Ed25519PrivateKey.__from_preserve__ = toEd25519PrivateKey;

export function fromEd25519PrivateKey<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Ed25519PrivateKey): _.Value<_embedded> {return _.Record(__lit10, [_v["q"], _v["d"]]);}


