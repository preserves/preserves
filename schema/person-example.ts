////------------------------------------------------------------
//// person-example.ts

import * as _ from "@preserves/core";

export const $date = _.Symbol.for("date");
export const $person = _.Symbol.for("person");

let __schema: _.Value | null = null;

export function _schema() {
    if (__schema === null) {
        __schema = _.decode<_.GenericEmbedded>(_.Bytes.fromHex("b4b306736368656d61b7b30776657273696f6eb00101b30b646566696e6974696f6e73b7b30444617465b4b303726563b4b3036c6974b3046461746584b4b3057475706c65b5b4b3056e616d6564b30479656172b4b30461746f6db30d5369676e6564496e74656765728484b4b3056e616d6564b3056d6f6e7468b4b30461746f6db30d5369676e6564496e74656765728484b4b3056e616d6564b303646179b4b30461746f6db30d5369676e6564496e74656765728484848484b306506572736f6eb4b303726563b4b3036c6974b306706572736f6e84b4b3057475706c65b5b4b3056e616d6564b3046e616d65b4b30461746f6db306537472696e678484b4b3056e616d6564b3086269727468646179b4b303726566b584b30444617465848484848484b30c656d62656464656454797065808484"));
    };
    return __schema;
}

export const _imports = {}


export type Date = (
    {"year": number, "month": number, "day": number} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);

export type Person = (
    {"name": string, "birthday": Date} &
    _.Preservable<any> &
    _.PreserveWritable<any> &
    {
        __as_preserve__<_embedded extends _.Embeddable = _.GenericEmbedded>(): _.Value<_embedded>
    }
);


export function Date({year, month, day}: {year: number, month: number, day: number}): Date {
    return {
        "year": year,
        "month": month,
        "day": day,
        __as_preserve__() {return fromDate(this);},
        __preserve_on__(e) { e.push(fromDate(this)); },
        __preserve_text_on__(w) { w.push(fromDate(this)); }
    };
}

Date.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Date")};
}

export function Person({name, birthday}: {name: string, birthday: Date}): Person {
    return {
        "name": name,
        "birthday": birthday,
        __as_preserve__() {return fromPerson(this);},
        __preserve_on__(e) { e.push(fromPerson(this)); },
        __preserve_text_on__(w) { w.push(fromPerson(this)); }
    };
}

Person.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Person")};
}

export function asDate<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Date {
    let result = toDate(v);
    if (result === void 0) throw new TypeError(`Invalid Date: ${_.stringify(v)}`);
    return result;
}

export function toDate<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Date {
    let result: undefined | Date;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $date) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = typeof v[0] === 'number' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = typeof v[1] === 'number' ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    let _tmp3: (number) | undefined;
                    _tmp3 = typeof v[2] === 'number' ? v[2] : void 0;
                    if (_tmp3 !== void 0) {
                        result = {
                            "year": _tmp1,
                            "month": _tmp2,
                            "day": _tmp3,
                            __as_preserve__() {return fromDate(this);},
                            __preserve_on__(e) { e.push(fromDate(this)); },
                            __preserve_text_on__(w) { w.push(fromDate(this)); }
                        };
                    };
                };
            };
        };
    };
    return result;
}

Date.__from_preserve__ = toDate;

export function fromDate<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Date): _.Value<_embedded> {return _.Record($date, [_v["year"], _v["month"], _v["day"]]);}

export function asPerson<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): Person {
    let result = toPerson(v);
    if (result === void 0) throw new TypeError(`Invalid Person: ${_.stringify(v)}`);
    return result;
}

export function toPerson<_embedded extends _.Embeddable = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Person {
    let result: undefined | Person;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $person) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (Date) | undefined;
                _tmp2 = toDate(v[1]);
                if (_tmp2 !== void 0) {
                    result = {
                        "name": _tmp1,
                        "birthday": _tmp2,
                        __as_preserve__() {return fromPerson(this);},
                        __preserve_on__(e) { e.push(fromPerson(this)); },
                        __preserve_text_on__(w) { w.push(fromPerson(this)); }
                    };
                };
            };
        };
    };
    return result;
}

Person.__from_preserve__ = toPerson;

export function fromPerson<_embedded extends _.Embeddable = _.GenericEmbedded>(_v: Person): _.Value<_embedded> {return _.Record($person, [_v["name"], fromDate<_embedded>(_v["birthday"])]);}


